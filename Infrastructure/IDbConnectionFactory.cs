﻿using System;
using System.Data;

namespace Infrastructure
{
    public interface IDbConnectionFactory: IDisposable
    {
        IDbTransaction GetCurrentTransaction();
        void DisposeTransaction();
        IDbConnection ReturnOpenedConnection();
        IDbTransaction GetOrRenewTransaction();

    }
}
