﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;

using Dapper;
using Dapper.Oracle;

using Domain.Core.Attributes;
using Domain.Core.Exceptions;
using Domain.Core.Interfaces;

using Infrastructure.Configurations;
using Infrastructure.Exceptions;

using Oracle.ManagedDataAccess.Client;

using OracleConfig = OracleSqlBuilder.Helpers.OracleConfiguration;

namespace Infrastructure.StoredProcesses
{
    /// <inheritdoc cref="IStoredFunction"/>
    internal class StoredFunction : IStoredFunction
    {
        private readonly string _functionName;
        private OracleDynamicParameters _oracleDynamicParameters;
        private IStoredFunctionInput _storedFunctionInput;
        private readonly IDbConnection _dbConnection;
        private const string RETURN_PROPERTY_NAME = "returnProperty";

        public StoredFunction(IDbConnection dbConnection, string functionName)
        {
            _dbConnection = dbConnection;
            _functionName = functionName;
            _oracleDynamicParameters = new OracleDynamicParameters();

            DapperConfiguration.Initialize();
        }

        /// <inheritdoc/>
        public TStoredFunctionOutput WithInput<TStoredFunctionOutput>(IStoredFunctionInput input)
        {
            try
            {
                _storedFunctionInput = input;

                AddOutputOracleParameters<TStoredFunctionOutput>();
                AddInputOracleParameters();
                _dbConnection.Execute(_functionName, param: _oracleDynamicParameters, commandType: CommandType.StoredProcedure);

                return MapOracleParametersToOutput<TStoredFunctionOutput>();
            }
            catch (InvalidOracleTypeException ex)
            {
                throw new DatabaseException($"Something wrong while converting {ex.ParameterDirection} parameter type into database type", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException($"Something wrong while executing the procedure", ex);
            }
        }

        /// <summary>
        /// To map result values from <paramref name="oracleDynamicParameters"/> into <paramref name="storedProcedureObject"/> matching name property
        /// </summary>
        /// <typeparam name="TStoredprocedureIn"></typeparam>
        /// <param name="oracleDynamicParameters"></param>
        /// <param name="storedProcedureObject"></param>
        /// <returns></returns>
        private TStoredFunctionOutput MapOracleParametersToOutput<TStoredFunctionOutput>()
        {
            try
            {

                MethodInfo method = typeof(OracleDynamicParameters).GetMethod("Get");
                MethodInfo generic = method.MakeGenericMethod(typeof(TStoredFunctionOutput));
                var result = generic.Invoke(_oracleDynamicParameters, new object[] { RETURN_PROPERTY_NAME });

                if (result is null)
                    throw new ApplicationException("Nothing was return");

                return (TStoredFunctionOutput)result;

            }
            catch (ApplicationException ex)
            {
                throw new InvalidOracleTypeException("Error while casting oracle dbType into c# type", ParameterDirection.Output, ex);
            }
        }

        private void AddInputOracleParameters()
        {
            foreach (var property in _storedFunctionInput.GetType().GetProperties())
            {
                StoredItemParameterAttribute storedProcedureParameterAttribute = property
                    .GetCustomAttributes(typeof(StoredItemParameterAttribute), true)
                    .Cast<StoredItemParameterAttribute>()
                    .FirstOrDefault();

                // Default values
                int size = OracleConfig.PARAMETER_DEFAUT_SIZE;
                ParameterDirection parameterDirection = ParameterDirection.Input;

                // Récupération depuis l'attribut [StoredProcedureParameter(size: ..., parameterDirection: ...)]
                if (storedProcedureParameterAttribute != null)
                    size = storedProcedureParameterAttribute.Size;

                string parameterName = property.Name;

                // Récupération depuis la valeur de la propriété
                object value = property.GetValue(_storedFunctionInput);

                // Récupération du DbType type oracle à partir du type de la propriété (string -> OracleMappingType.VarChar2)
                OracleMappingType type = DbTypeHandler.GetOracleMappingTypeFromType(property.PropertyType);

                _oracleDynamicParameters.Add(parameterName, value, type, parameterDirection, size);
            }
        }

        private void AddOutputOracleParameters<TStoredFunctionOutput>()
        {
            // Default values
            int size = OracleConfig.PARAMETER_DEFAUT_SIZE;
            ParameterDirection parameterDirection = ParameterDirection.ReturnValue;

            // Récupération du DbType type oracle à partir du type de la propriété (string -> OracleMappingType.VarChar2)
            OracleMappingType type = DbTypeHandler.GetOracleMappingTypeFromType(typeof(TStoredFunctionOutput));
            _oracleDynamicParameters.Add(RETURN_PROPERTY_NAME, "", type, parameterDirection, size);
        }
    }
}