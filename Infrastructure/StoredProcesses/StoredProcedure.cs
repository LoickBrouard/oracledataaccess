﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

using Dapper;
using Dapper.Oracle;

using Domain.Core.Attributes;
using Domain.Core.Exceptions;
using Domain.Core.Interfaces;

using Infrastructure.Configurations;
using Infrastructure.Exceptions;

using Oracle.ManagedDataAccess.Client;

using OracleConfig = OracleSqlBuilder.Helpers.OracleConfiguration;

namespace Infrastructure.Storedprocedures
{
    /// <inheritdoc cref="IStoredProcedure"/>
    internal class StoredProcedure<TStoredProcedureInput, TStoredProcedureOutput> : IStoredProcedureOnGoing<TStoredProcedureInput, TStoredProcedureOutput>
        where TStoredProcedureInput : IStoredProcedureInput
        where TStoredProcedureOutput : IStoredProcedureOutput
    {
        private readonly string _procedureName;
        private OracleDynamicParameters _oracleDynamicParameters;
        private readonly IDbConnection _dbConnection;
        private IStoredProcedureInput _storedProcedureInput;
        private IStoredProcedureOutput _storedProcedureOutput;


        /// <summary>
        /// Constructor mapping the connection and transaction
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <param name="procedureName"></param>
        /// <returns></returns>
        public StoredProcedure(IDbConnection dbConnection, string procedureName)
        {
            _dbConnection = dbConnection;
            _procedureName = procedureName;
            _oracleDynamicParameters = new OracleDynamicParameters();

            DapperConfiguration.Initialize();
        }


        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public IStoredProcedureOnGoing<TStoredProcedureInput, TStoredProcedureOutput> WithInput(IStoredProcedureInput input)
        {
            try
            {
                if (input is null)
                    throw new InputNullException(nameof(input));

                _storedProcedureInput = input;
                AddInputOracleParameters();
                return this;
            }
            catch (InputNullException ex)
            {
                throw new DatabaseException($"The parameter {ex.InputName} is null or empty", ex);
            }
            catch (InvalidOracleTypeException ex)
            {
                throw new DatabaseException($"Something wrong while converting {ex.ParameterDirection} parameter type into database type", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException($"Something wrong while executing the procedure", ex);
            }
        }

        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public TStoredProcedureOutput Execute()
        {
            try
            {
                _storedProcedureOutput = InstanceOf<TStoredProcedureOutput>();
                AddOutputOracleParameters();

                _dbConnection.Execute(_procedureName, param: _oracleDynamicParameters, commandType: CommandType.StoredProcedure);

                return (TStoredProcedureOutput)MapOracleParametersToOutput();

            }
            catch (InvalidOracleTypeException ex)
            {
                throw new DatabaseException($"Something wrong while converting {ex.ParameterDirection} parameter type into database type", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException($"Something wrong while executing the procedure", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException($"Unhandled exception (see inner exception for more detail)", ex);
            }
        }

        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public TStoredProcedureOutput ExecuteWithRefCursor<TRefCursor>()
        {
            try
            {
                _storedProcedureOutput = InstanceOf<TStoredProcedureOutput>();
                AddOutputOracleParameters();

                IEnumerable<TRefCursor> returnValue = _dbConnection.Query<TRefCursor>(_procedureName, param: _oracleDynamicParameters, commandType: CommandType.StoredProcedure);

                MapRefCursorToOutput(returnValue);
                return (TStoredProcedureOutput)MapOracleParametersToOutput();

            }
            catch (InvalidOracleTypeException ex)
            {
                throw new DatabaseException($"Something wrong while converting {ex.ParameterDirection} parameter type into database type", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException($"Something wrong while executing the procedure", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException($"Unhandled exception (see inner exception for more detail)", ex);
            }
        }

        private static IStoredProcedureOutput InstanceOf<TStoredProcedureOutput>() where TStoredProcedureOutput : IStoredProcedureOutput
        {
            return (IStoredProcedureOutput)Activator.CreateInstance(typeof(TStoredProcedureOutput));
        }

        /// <summary>
        /// To map result values from <paramref name="oracleDynamicParameters"/> into <paramref name="storedProcedureObject"/> matching name property
        /// </summary>
        /// <typeparam name="TStoredprocedureIn"></typeparam>
        /// <param name="oracleDynamicParameters"></param>
        /// <param name="storedProcedureObject"></param>
        /// <returns></returns>
        private IStoredProcedureOutput MapOracleParametersToOutput()
        {
            try
            {
                foreach (var property in _storedProcedureOutput.GetType().GetProperties())
                {
                    RefCursorAttribute refCursorParameterAttribute = property
                        .GetCustomAttributes(typeof(RefCursorAttribute), true)
                        .Cast<RefCursorAttribute>()
                        .FirstOrDefault();

                    MethodInfo method = typeof(OracleDynamicParameters).GetMethod("Get");
                    MethodInfo generic;
                    // Récupération depuis l'attribut [RefCursor()]
                    if (refCursorParameterAttribute != null)
                        generic = method.MakeGenericMethod(DbTypeHandler.GetOracleRefCursor());
                    else
                        generic = method.MakeGenericMethod(property.PropertyType);
                    var result = generic.Invoke(_oracleDynamicParameters, new object[] { property.Name });

                    if (result == null)
                        continue;

                    property.SetValue(_storedProcedureOutput, result);
                }

                return _storedProcedureOutput;
            }
            catch (ApplicationException ex)
            {
                throw new InvalidOracleTypeException("Error while casting oracle dbType into c# type", ParameterDirection.Output, ex);
            }
        }

        private void AddOutputOracleParameters()
        {
            foreach (var property in _storedProcedureOutput.GetType().GetProperties())
            {
                StoredItemParameterAttribute storedProcedureParameterAttribute = property
                    .GetCustomAttributes(typeof(StoredItemParameterAttribute), true)
                    .Cast<StoredItemParameterAttribute>()
                    .FirstOrDefault();

                RefCursorAttribute RefCursorAttribute = property
                    .GetCustomAttributes(typeof(RefCursorAttribute), true)
                    .Cast<RefCursorAttribute>()
                    .FirstOrDefault();

                // Default values
                int size = OracleConfig.PARAMETER_DEFAUT_SIZE;
                ParameterDirection parameterDirection = ParameterDirection.Output;

                // Récupération depuis l'attribut [StoredProcedureParameter(size: ...)]
                if (storedProcedureParameterAttribute != null)
                    size = storedProcedureParameterAttribute.Size;

                string parameterName = property.Name;

                // Récupération depuis la valeur de la propriété
                object value = property.GetValue(_storedProcedureOutput);

                OracleMappingType type;

                if (RefCursorAttribute != null)
                    type = DbTypeHandler.GetOracleRefCursorType();
                else
                    type = DbTypeHandler.GetOracleMappingTypeFromType(property.PropertyType);

                _oracleDynamicParameters.Add(parameterName, value, type, parameterDirection, size);

            }
        }

        private void AddInputOracleParameters()
        {
            foreach (var property in _storedProcedureInput.GetType().GetProperties())
            {
                StoredItemParameterAttribute storedProcedureParameterAttribute = property
                    .GetCustomAttributes(typeof(StoredItemParameterAttribute), true)
                    .Cast<StoredItemParameterAttribute>()
                    .FirstOrDefault();

                // Default values
                int size = OracleConfig.PARAMETER_DEFAUT_SIZE;
                ParameterDirection parameterDirection = ParameterDirection.Input;

                // Récupération depuis l'attribut [StoredProcedureParameter(size: ..., parameterDirection: ...)]
                if (storedProcedureParameterAttribute != null)
                    size = storedProcedureParameterAttribute.Size;

                string parameterName = property.Name;

                // Récupération depuis la valeur de la propriété
                object value = property.GetValue(_storedProcedureInput);

                // Récupération du DbType type oracle à partir du type de la propriété (string -> OracleMappingType.VarChar2)
                OracleMappingType type = DbTypeHandler.GetOracleMappingTypeFromType(property.PropertyType);

                _oracleDynamicParameters.Add(parameterName, value, type, parameterDirection, size);
            }
        }

        private void MapRefCursorToOutput<TRefCursor>(IEnumerable<TRefCursor> returnValue)
        {
            IEnumerable<PropertyInfo> refCursorProperties = _storedProcedureOutput
                .GetType()
                .GetProperties()
                .Where(p =>
                    p.PropertyType == typeof(IEnumerable<TRefCursor>)
                    && p.GetCustomAttributes(typeof(RefCursorAttribute), true) != null).ToList();

            if (!refCursorProperties.Any())
                return;

            PropertyInfo refCursorProperty = refCursorProperties.First();

            if (typeof(TRefCursor) != typeof(object))
                refCursorProperty.SetValue(_storedProcedureOutput, returnValue);
            else
            {
                List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();

                foreach (IDictionary<string, object> innerDictionnary in ((IList<object>)returnValue).Cast<IDictionary<string, object>>().AsList())
                {
                    Dictionary<string, object> tempDict = new Dictionary<string, object>();
                    foreach (KeyValuePair<string, object> inner in innerDictionnary)
                    {
                        tempDict.Add(inner.Key, inner.Value);
                    }
                    result.Add(tempDict);
                }
                refCursorProperty.SetValue(_storedProcedureOutput, result);

            }
        }
    }
}
