﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using Domain.Core.Exceptions;

using Oracle.ManagedDataAccess.Client;

namespace Infrastructure
{
    internal class DbConnectionFactory : IDbConnectionFactory
    {
        private bool disposedValue;
        private IDbConnection dbConnection { get; set; }
        private IDbTransaction dbTransaction { get; set; }
        private readonly string _connectionString;

        public DbConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDbTransaction GetCurrentTransaction() => dbTransaction;

        public IDbTransaction GetOrRenewTransaction()
        {
            try
            {
                if (dbTransaction == null)
                    dbTransaction = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);
                return dbTransaction;
            }
            catch (OracleException ex)
            {
                throw new DatabaseAccessException("Erreur lors de la création de la transaction", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseAccessException("Exception inattendue lors de la création de la transaction", ex);
            }
        }

        public IDbConnection ReturnOpenedConnection()
        {
            if (dbConnection is null)
                dbConnection = new OracleConnection(_connectionString);

            if (dbConnection.State is ConnectionState.Closed)
            {
                dbConnection.Open();
                Trace.WriteLine("Db Connection openned");
            }
            
            return dbConnection;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    if (dbConnection != null && dbConnection.State != ConnectionState.Closed)
                    {
                        Trace.WriteLine("Db Connection closed");
                        dbConnection.Close();
                    }
                }
                if (dbConnection != null && dbConnection.State != ConnectionState.Closed)
                {
                    Trace.WriteLine("Db Connection closed");
                    dbConnection.Close();
                }
                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public void DisposeTransaction()
        {
            dbTransaction = null;
        }
    }
}
