﻿using Dapper;

using Oracle.ManagedDataAccess.Client;

using System.Data;

namespace Infrastructure.Configurations
{
    internal class NullableBoolTypeHandler : SqlMapper.TypeHandler<bool?>
    {
        public override void SetValue(IDbDataParameter parameter, bool? value)
        {
            parameter.DbType = DbType.String;
            if (value == null)
                parameter.Value = null;
            if (value == true)
                parameter.Value = "1";
            else
                parameter.Value = "0";
        }

        public override bool? Parse(object value)
        {
            if (value == null)
                return null;
            if (value.ToString() == "1")
                return true;
            return false;
        }
    }

    internal class BoolTypeHandler : SqlMapper.TypeHandler<bool>
    {
        public override void SetValue(IDbDataParameter parameter, bool value)
        {
            parameter.DbType = DbType.String;
            if (value == true)
                parameter.Value = "1";
            else
                parameter.Value = "0";
        }

        public override bool Parse(object value)
        {
            if (value.ToString() == "1")
                return true;
            return false;
        }
    }
}
