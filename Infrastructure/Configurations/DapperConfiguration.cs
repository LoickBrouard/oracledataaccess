﻿using Dapper;

namespace Infrastructure.Configurations
{
    internal class DapperConfiguration
    {
        public static void Initialize()
        {
            // Suppression des handlers par défaut
            SqlMapper.RemoveTypeMap(typeof(bool?));
            SqlMapper.RemoveTypeMap(typeof(bool));

            // Ajout des handlers personalisés
            SqlMapper.AddTypeHandler(typeof(bool?), new NullableBoolTypeHandler());
            SqlMapper.AddTypeHandler(typeof(bool), new BoolTypeHandler());

        }
    }
}
