﻿using Dapper.Oracle;

using Infrastructure.Exceptions;

using Oracle.ManagedDataAccess.Types;

using System;
using System.Collections.Generic;
using System.Data;

namespace Infrastructure.Configurations
{
    internal static class DbTypeHandler
    {
        private static Dictionary<Type, OracleMappingType> _typeTransformer { get; } = new Dictionary<Type, OracleMappingType>()
        {
            { typeof(short) , OracleMappingType.Int16 },
            { typeof(short?) , OracleMappingType.Int16 },
            { typeof(string) , OracleMappingType.Varchar2 },
            { typeof(decimal) , OracleMappingType.Decimal },
            { typeof(decimal?) , OracleMappingType.Decimal },
            { typeof(double) , OracleMappingType.Double },
            { typeof(double?) , OracleMappingType.Double },
            { typeof(byte) , OracleMappingType.Byte },
            { typeof(byte?) , OracleMappingType.Byte },
            { typeof(int?) , OracleMappingType.Int32 },
            { typeof(int) , OracleMappingType.Int32 },
            { typeof(char) , OracleMappingType.Char },
            { typeof(char?) , OracleMappingType.Char },
            { typeof(DateTime) , OracleMappingType.Date },
            { typeof(DateTime?) , OracleMappingType.Date },
            { typeof(long) , OracleMappingType.Long },
            { typeof(long?) , OracleMappingType.Long },
            { typeof(bool?) , OracleMappingType.Byte },
            { typeof(bool) , OracleMappingType.Byte }
        };

        private static Dictionary<Type, DbType> _dbtypeTransformer { get; } = new Dictionary<Type, DbType>()
        {
            { typeof(short) , DbType.Int16 },
            { typeof(short?) , DbType.Int16 },
            { typeof(string) , DbType.String },
            { typeof(decimal) , DbType.Decimal },
            { typeof(decimal?) , DbType.Decimal },
            { typeof(double) , DbType.Double },
            { typeof(double?) , DbType.Double },
            { typeof(byte) , DbType.Byte },
            { typeof(byte?) , DbType.Byte },
            { typeof(int?) , DbType.Int32 },
            { typeof(int) , DbType.Int32 },
            { typeof(char) , DbType.String },
            { typeof(char?) , DbType.String },
            { typeof(DateTime) , DbType.Date },
            { typeof(DateTime?) , DbType.Date },
            { typeof(long) , DbType.VarNumeric },
            { typeof(long?) , DbType.VarNumeric },
            { typeof(bool?) , DbType.Byte },
            { typeof(bool) , DbType.Byte }
        };

        public static DbType GetDbMappingTypeFromType(Type type)
        {
            if (!_dbtypeTransformer.ContainsKey(type))
                throw new InvalidOracleTypeException($"Type {type.Name} is unsupported", ParameterDirection.Input);

            _dbtypeTransformer.TryGetValue(type, out DbType value);
            return value;
        }

        public static OracleMappingType GetOracleMappingTypeFromType(Type type)
        {
            if (!_typeTransformer.ContainsKey(type))
                throw new InvalidOracleTypeException($"Type {type.Name} is unsupported", ParameterDirection.Input);

            _typeTransformer.TryGetValue(type, out OracleMappingType value);
            return value;
        }
        
        public static OracleMappingType GetOracleRefCursorType()
        {
            return OracleMappingType.RefCursor;
        }
        public static Type GetOracleRefCursor()
        {
            return typeof(OracleRefCursor);
        }
    }
}
