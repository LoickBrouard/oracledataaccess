﻿using System;

namespace Infrastructure.Exceptions
{
	/// <summary>
	/// Exception whent the input furnish to the library is null or empty
	/// </summary>
	[Serializable]
	public class InputNullException : Exception
	{
		public string InputName { get; set; }

		public InputNullException(string inputName) : base() { InputName = inputName; }
	}
}
