﻿using System;
using System.Data;

namespace Infrastructure.Exceptions
{
    public class InvalidOracleTypeException : Exception
    {
        public ParameterDirection ParameterDirection { get; set; }

        public InvalidOracleTypeException(string message, ParameterDirection parameterDirection) : base(message) { ParameterDirection = parameterDirection; }
        public InvalidOracleTypeException(string message, ParameterDirection parameterDirection, Exception innerException) : base(message, innerException) { ParameterDirection = parameterDirection; }
    }
}