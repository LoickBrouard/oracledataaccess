﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Dapper;

using Domain.Core.Exceptions;
using Domain.Core.Interfaces;
using Domain.Core.Primitives;

using Infrastructure.Repositories;
using Infrastructure.Storedprocedures;
using Infrastructure.StoredProcesses;

using Oracle.ManagedDataAccess.Client;

namespace Infrastructure
{
    /// <inheritdoc cref="IUnitOfWork"/>
    /// <exception cref="DatabaseAccessException"></exception>
    public class UnitOfWork : IUnitOfWork
    {
        private bool disposed = false;
        private Dictionary<Type, object> _repositories;
        readonly IDbConnectionFactory _dbConnectionFactory;

        /// <inheritdoc/>
        public UnitOfWork(string connectionString, IDbConnectionFactory dbConnectionFactory = null)
        {
            try
            {
                Trace.WriteLine("UnitOfWork instanciated");

                _dbConnectionFactory = dbConnectionFactory ?? new DbConnectionFactory(connectionString);

            }
            catch (OracleException ex)
            {
                throw new DatabaseAccessException("Erreur lors de la connection avec oracle", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseAccessException("Exception inattendue lors de la connection avec Oracle", ex);
            }
        }

        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity
        {
            try
            {
                if (_repositories == null)
                    _repositories = new Dictionary<Type, object>();

                Type entityType = typeof(TEntity);

                if (!_repositories.ContainsKey(entityType))
                    _repositories[entityType] = new GenericRepository<TEntity>(_dbConnectionFactory.ReturnOpenedConnection(), _dbConnectionFactory.GetOrRenewTransaction());


                return (IRepository<TEntity>)_repositories[entityType];
            }
            catch (Exception ex)
            {
                throw new DatabaseException($"Exception inattendue lors de la création du repository de l'entité {typeof(TEntity).Name}", ex);
            }
        }

        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public IStoredProcedureOnGoing<TStoredProcedureInput, TStoredProcedureOutput> GetStoredProcedure<TStoredProcedureInput, TStoredProcedureOutput>(string procedureName) 
            where TStoredProcedureInput : IStoredProcedureInput
            where TStoredProcedureOutput : IStoredProcedureOutput
        {
            try
            {
                return new StoredProcedure<TStoredProcedureInput, TStoredProcedureOutput>(_dbConnectionFactory.ReturnOpenedConnection(), procedureName);
            }
            catch (Exception ex)
            {
                throw new DatabaseException($"Exception inattendue lors de la création de la procédure stockée {procedureName}", ex);
            }
        }

        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public IStoredFunction ExecuteStoredFunction(string functionName)
        {
            try
            {
                return new StoredFunction(_dbConnectionFactory.ReturnOpenedConnection(), functionName);
            }
            catch (Exception ex)
            {
                throw new DatabaseException($"Exception inattendue lors de la création de la procédure stockée {functionName}", ex);
            }
        }

        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public IEnumerable<TReturnObject> QueryFromRawSql<TReturnObject>(string sqlQuery, object parameters, TReturnObject returnModel)
        {
            if (string.IsNullOrWhiteSpace(sqlQuery))
                throw new DatabaseException($"{nameof(sqlQuery)} must not be empty");

            try
            {
                return _dbConnectionFactory.ReturnOpenedConnection().Query<TReturnObject>(sql: sqlQuery, param: parameters, transaction: _dbConnectionFactory.GetCurrentTransaction());
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public void ExecuteFromRawSql(string sqlQuery, object parameters)
        {
            if (string.IsNullOrWhiteSpace(sqlQuery))
                throw new DatabaseException($"{nameof(sqlQuery)} must not be empty");

            try
            {

                if (_dbConnectionFactory.ReturnOpenedConnection().Execute(sql: sqlQuery, param: parameters, transaction: _dbConnectionFactory.GetCurrentTransaction()) <= 0)
                    throw new DatabaseException("Aucune entrée n'a été modifiée dans la base de donnée");
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public void SaveChanges()
        {
            try
            {
                _dbConnectionFactory.GetCurrentTransaction()?.Commit();
                _dbConnectionFactory.DisposeTransaction();
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors du commit", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Exception inattendue lors du commit", ex);
            }
        }

        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public void Rollback()
        {
            try
            {
                _dbConnectionFactory.GetCurrentTransaction()?.Rollback();
                _dbConnectionFactory.DisposeTransaction();
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors du rollback", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Exception inattendue lors du rollback", ex);
            }
        }

        /// <inheritdoc/>
        /// <exception cref="DatabaseException" />
        public DateTime GetDatabaseDateTime()
        {
            try
            {
                return _dbConnectionFactory.ReturnOpenedConnection().ExecuteScalar<DateTime>($"SELECT {OracleSqlBuilder.Helpers.OracleConfiguration.SERVER_DATE_NAME} from dual");
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Exception inattendue lors de l'interrogation de la base de donnée", ex);
            }
        }

        /// <inheritdoc/>
        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _dbConnectionFactory.Dispose();
                }
                _dbConnectionFactory.Dispose();
            }
            disposed = true;
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            Trace.WriteLine("UnitOfWork Disposed");
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
