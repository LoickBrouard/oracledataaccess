﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using Dapper;

#if NETCOREAPP
using Domain.Core.Attributes;
#endif
using Domain.Core.Exceptions;
using Domain.Core.Interfaces;
using Domain.Core.Primitives;

using Infrastructure.Configurations;
using Infrastructure.Helpers;

using Oracle.ManagedDataAccess.Client;

using OracleSqlBuilder;
using OracleSqlBuilder.Exceptions;
using OracleSqlBuilder.Models;

using static Dapper.SqlMapper;

namespace Infrastructure.Repositories
{

    /// <inheritdoc />
    public sealed class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {

        private readonly IDbConnection _dbConnection;
        private readonly IDbTransaction _dbTransaction;

        /// <summary>
        /// Constructor mapping the connection and transaction and
        /// Configuration of Dapper configuration
        /// </summary>
        /// <param name="dbConnection"></param>
        /// <param name="dbTransaction"></param>
        public GenericRepository(IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            _dbConnection = dbConnection;
            _dbTransaction = dbTransaction;

            DapperConfiguration.Initialize();
        }

        /// <inheritdoc />
#if NETCOREAPP
        public TEntity? GetById(object id)
#else
        public TEntity GetById(object id)
#endif
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                    .Select()
                    .WhereId(id)
                    .Build();

#if NETCOREAPP

                return _dbConnection.Query<TEntity>(query.QueryString, query.WhereFilter)?.FirstOrDefault() ?? null;
#else
                IEnumerable<TEntity> entities = _dbConnection.Query<TEntity>(query.QueryString, query.WhereFilter);
                if (entities.Any())
                    return entities.First();
                return null;
#endif


            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
#if NETCOREAPP
        public TEntity? GetById(TEntity entity)
#else
        public TEntity GetById(TEntity entity)
#endif
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                    .Select()
                    .WhereIds(entity)
                    .Build();

#if NETCOREAPP

                return _dbConnection.Query<TEntity>(query.QueryString, query.WhereFilter)?.FirstOrDefault() ?? null;
#else
                IEnumerable<TEntity> entities = _dbConnection.Query<TEntity>(query.QueryString, query.WhereFilter);
                if (entities.Any())
                    return entities.First();
                return null;
#endif


            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
#if NETCOREAPP
        public TEntity? GetFirst(Expression<Func<TEntity, bool>> expression)
#else
        public TEntity GetFirst(Expression<Func<TEntity, bool>> expression)
#endif
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                    .Select()
                    .Where(expression)
                    .Build();

#if NETCOREAPP

                return _dbConnection.Query<TEntity>(query.QueryString, query.WhereFilter)?.FirstOrDefault() ?? null;
#else
                IEnumerable<TEntity> entities = _dbConnection.Query<TEntity>(query.QueryString, query.WhereFilter);
                if (entities.Any())
                    return entities.First();
                return null;
#endif


            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
        public IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> expression)
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                    .Select()
                    .Where(expression)
                    .Build();

                return _dbConnection.Query<TEntity>(query.QueryString, query.WhereFilter);
            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
        public int Count()
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                    .Count()
                    .Build();

                return _dbConnection.ExecuteScalar<int>(query.QueryString, transaction: _dbTransaction);
            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }

        }

        /// <inheritdoc />
        public int Count(Expression<Func<TEntity, bool>> expression)
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                   .Count()
                   .Where(expression)
                   .Build();

                return _dbConnection.ExecuteScalar<int>(query.QueryString, query.WhereFilter, transaction: _dbTransaction);
            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
        public TReturnType Max<TReturnType>(string propertySearched)
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                   .Max(propertySearched)
                   .Build();
                return _dbConnection.ExecuteScalar<TReturnType>(query.QueryString, transaction: _dbTransaction);
            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
        public TReturnType Max<TReturnType>(string propertySearched, Expression<Func<TEntity, bool>> expression)
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                   .Max(propertySearched)
                   .Where(expression)
                   .Build();
                return _dbConnection.ExecuteScalar<TReturnType>(query.QueryString, query.WhereFilter, transaction: _dbTransaction);
            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
        bool IRepository<TEntity>.Exists(object primaryKey)
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                   .Count()
                   .WhereId(primaryKey)
                   .Build();

                return _dbConnection.ExecuteScalar<int>(query.QueryString, query.WhereFilter, transaction: _dbTransaction) == 1;
            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
        public IEnumerable<TEntity> GetAll()
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                    .Select()
                    .Build();

                return _dbConnection.Query<TEntity>(query.QueryString);
            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
        public TEntity Insert(TEntity entity)
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                .Add()
                .ReturnElementsInsertedAutomatically()
                .Build();

                DynamicParameters param = InsertQueryExtension.ConfigureParameters(entity);

                if (_dbConnection.Execute(sql: query.QueryString, param: param, transaction: _dbTransaction) <= 0)
                    throw new DatabaseException("Aucune entrée n'a été insérée en base de donnée");

                return param.MapEntityFromParameterReturned(entity);

            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'insert en base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
        public TEntity Update(TEntity entity)
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                   .Update()
                   .WhereIds(entity)
                   .Build();

                if (_dbConnection.Execute(query.QueryString, entity, transaction: _dbTransaction) <= 0)
                    throw new DatabaseException("Aucune entrée n'a été mise à jour en base de donnée");
                return entity;
            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
        public void Delete(TEntity entity)
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                    .Delete()
                    .WhereIds(entity)
                    .Build();

                if (_dbConnection.Execute(query.QueryString, query.WhereFilter, transaction: _dbTransaction) <= 0)
                    throw new DatabaseException("Aucune entrée n'a été supprimée de la base de donnée");
            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }

        /// <inheritdoc />
        public IEnumerable<TEntity> GetJoin<TJoinedEntity>(Func<TEntity, TJoinedEntity, TEntity> mapFunction, Expression<Func<TEntity, TJoinedEntity, bool>> junction, Join joinType = Join.Left, Expression<Func<TEntity, bool>> expression = null) where TJoinedEntity : Entity
        {
            try
            {
                Query query = OracleQueryBuilder<TEntity>
                    .SelectJoin<TJoinedEntity>()
                    .Join(joinType, junction)
                    .Where(expression)
                    .Build();

                MethodInfo generic = typeof(SqlMapper).GetMethods().FirstOrDefault(
                    m => m.Name.Equals("Query", StringComparison.OrdinalIgnoreCase) &&
                    m.IsGenericMethod &&
                    m.GetGenericArguments().Length == 3)?
                    .MakeGenericMethod(typeof(TEntity), typeof(TJoinedEntity), typeof(TEntity));

                IEnumerable<TEntity> result = (IEnumerable<TEntity>)generic.Invoke(_dbConnection,
                    new object[] { _dbConnection, query.QueryString, mapFunction, query.WhereFilter, _dbTransaction, Type.Missing, query.SplitOn, Type.Missing, Type.Missing });

                return result;
            }
            catch (SqlBuilderException ex)
            {
                throw new DatabaseException("Erreur lors de la construction de la query", ex);
            }
            catch (OracleException ex)
            {
                throw new DatabaseException("Erreur lors de l'interrogation de la base", ex);
            }
            catch (Exception ex)
            {
                throw new DatabaseException("Erreur inattendue", ex);
            }
        }
    }
}
