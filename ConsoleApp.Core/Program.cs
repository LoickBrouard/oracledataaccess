﻿// See https://aka.ms/new-console-template for more information
using Business.Entities.StoredProcedures;

using Domain.Core.Exceptions;
using Domain.Core.Interfaces;

using Infrastructure;

Run();

partial class Program
{
    private static IUnitOfWork _unitOfWork;

    private static string connectionString = "Data Source=xxxxxxxxxx;Password=xxxxxxxxxx;User ID=xxxxxxxxxx";
    public static void Run()
    {
        try
        {
            // TODO : Refaire doc XML*
            using (IUnitOfWork _unitOfWork = new UnitOfWork(connectionString))
            {
StoredProcedureExampleOutput result = _unitOfWork
    .GetStoredProcedure<StoredProcedureExample, StoredProcedureExampleOutput>(StoredProcedureExample.PROCEDURE_NAME)
    .WithInput(new StoredProcedureExample()
    {
        InputString = "my input"
    })
    .Execute();
            }
        }
        catch (DatabaseException ex)
        {
            Console.WriteLine(ex.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);

            if (ex.InnerException != null)
                Console.WriteLine(ex.InnerException.Message);
        }
        finally
        {
            _unitOfWork.Rollback();
            Console.ReadKey();
        }
    }
}
