﻿using System;

using Domain.Core.Interfaces;

using Infrastructure;

using Test_Resources.FakeEntities;

using Moq;
using Xunit;
using System.Data.Common;
using Oracle.ManagedDataAccess.Client;

namespace Test_Infrastructure
{
    public class UnitOfWorkTests : IDisposable
    {
        #region Property  
        private bool disposedValue;
        private readonly IUnitOfWork _unitOfWork;
        private readonly Mock<IDbConnectionFactory> _mockDbConnectionFactory = new ();
        #endregion

        public UnitOfWorkTests()
        {
            _mockDbConnectionFactory.Setup(d => d.ReturnOpenedConnection()).Callback(() => { });
            _mockDbConnectionFactory.Setup(d => d.Dispose()).Callback(() => { });

            IDbConnectionFactory _dbConnectionFactory = _mockDbConnectionFactory.Object;
            
            _unitOfWork = new UnitOfWork("FakeConnectionString", _dbConnectionFactory);
        }

        [Fact]
        [Trait("REPOSITORY", "GET")]
        public void GetRepository_FirstCall_InstanciateNewRepository()
        {
            // Act
            IRepository<SimpleEntity> simpleEntityRepository = _unitOfWork.GetRepository<SimpleEntity>();

            // Assert
            Assert.NotNull(simpleEntityRepository);

        }

        [Fact]
        [Trait("REPOSITORY", "GET")]
        public void GetRepository_CalledTwice_SameInstanceIsReturned()
        {
            // Act
            IRepository<SimpleEntity> firstSimpleEntityRepository = _unitOfWork.GetRepository<SimpleEntity>();
            IRepository<SimpleEntity> secondSimpleEntityRepository = _unitOfWork.GetRepository<SimpleEntity>();

            // Assert
            Assert.Equal(firstSimpleEntityRepository, secondSimpleEntityRepository);

        }

        [Fact]
        [Trait("REPOSITORY", "GET")]
        public void GetRepository_CallTwoRepository_ReturnBoth()
        {
            // Act
            IRepository<SimpleEntity> simpleEntityRepository = _unitOfWork.GetRepository<SimpleEntity>();
            IRepository<ComplexEntity> complexEntityRepository = _unitOfWork.GetRepository<ComplexEntity>();

            // Assert
            Assert.NotNull(simpleEntityRepository);
            Assert.NotNull(complexEntityRepository);

        }

        #region DISPOSING METHODS

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _unitOfWork.Dispose();
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
