﻿using System;
using System.Collections.Generic;
using System.Linq;

using Domain.Core.Interfaces;
using Domain.Core.Primitives;

using Infrastructure;

using Test_Resources.Database;
using Test_Resources.FakeEntities;

using Xunit;

namespace Test_Infrastructure
{

    [Collection("Sequential")]
    [CollectionDefinition("Sequential", DisableParallelization = true)]
    public class SelectDatabaseTests : IDisposable
    {
        private bool disposedValue;
        private readonly DatabaseHandler _databaseHandler;
        private readonly IUnitOfWork _unitOfWork;

        public SelectDatabaseTests()
        {
            _databaseHandler = new DatabaseHandler(new Scheme[] { SimpleEntity.GetScheme() });
            _unitOfWork = new UnitOfWork(DatabaseHandler.CONNECTION_STRING);
            _unitOfWork.GetRepository<SimpleEntity>().Insert(new SimpleEntity() { FakeInt = 1, FakeString = "value" });
            _unitOfWork.SaveChanges();
        }

        #region SELECT FIRST


        [Fact]
        [Trait("SELECT", "FIRST")]
        public void SelectFirst_ValueExisting_ReturnEntity()
        {
            // Arrange
            SimpleEntity expectedEntity = new() { FakeInt = 1, FakeString = "value" };

            // Act
            SimpleEntity? simpleEntityFromDatabase = _unitOfWork.GetRepository<SimpleEntity>().GetFirst(s => s.FakeString == "value");

            // Assert
            Assert.Equal(expectedEntity, simpleEntityFromDatabase);

        }

        [Fact]
        [Trait("SELECT", "FIRST")]
        public void SelectFirst_ValueNotExisting_ReturnNull()
        {
            // Act
            SimpleEntity? simpleEntityFromDatabase = _unitOfWork.GetRepository<SimpleEntity>().GetFirst(s => s.FakeString == "NotExistingValue");

            // Assert
            Assert.Null(simpleEntityFromDatabase);

        }

        #endregion

        #region SELECT ALL


        [Fact]
        [Trait("SELECT", "ALL")]
        public void SelectAll_ValueExisting_ReturnEntity()
        {
            // Arrange
            SimpleEntity expectedEntity = new() { FakeInt = 1, FakeString = "value" };

            // Act
            IEnumerable<SimpleEntity> simpleEntitiesFromDatabase = _unitOfWork.GetRepository<SimpleEntity>().GetAll();

            // Assert
            Assert.Equal(1, simpleEntitiesFromDatabase.Count());
            Assert.Equal(new HashSet<SimpleEntity>() { expectedEntity }, new HashSet<SimpleEntity>(simpleEntitiesFromDatabase));

        }

        #endregion

        #region SELECT MANY

        [Fact]
        [Trait("SELECT", "MANY")]
        public void SelectALl_ValueExisting_ReturnValues()
        {
            // Arrange
            SimpleEntity expectedEntity = new() { FakeInt = 1, FakeString = "SelectALl_ValueExisting_ReturnValues1" };
            SimpleEntity expectedEntity1 = new() { FakeInt = 2, FakeString = "SelectALl_ValueExisting_ReturnValues2" };
            SimpleEntity expectedEntity2 = new() { FakeInt = 3, FakeString = "SelectALl_ValueExisting_ReturnValues3" };
            SimpleEntity unexpectedEntity = new() { FakeInt = 4, FakeString = "SelectALl_ValueExisting_ReturnValues4" };
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity);
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity1);
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity2);
            _unitOfWork.GetRepository<SimpleEntity>().Insert(unexpectedEntity);
            _unitOfWork.SaveChanges();

            IEnumerable<string> searchedValue = new string[2] { expectedEntity.FakeString, expectedEntity2.FakeString };

            // Act
            IEnumerable<SimpleEntity> simpleEntitiesFromDatabase = _unitOfWork
                .GetRepository<SimpleEntity>()
                .GetMany(s => s.In(s.FakeString, searchedValue));

            // Assert
            Assert.True(simpleEntitiesFromDatabase.Count() == searchedValue.Count());
            Assert.True(searchedValue.ToList().SequenceEqual(simpleEntitiesFromDatabase.Select(s => s.FakeString)));
        }

        [Fact]
        [Trait("SELECT", "MANY")]
        public void SelectALl_ValueWithDifferentCase_ReturnValuesWithCaseSensitiveTakeIntoAccount()
        {
            // Arrange
            SimpleEntity expectedEntity = new() { FakeInt = 1, FakeString = "RANDOM" };
            SimpleEntity expectedEntity1 = new() { FakeInt = 2, FakeString = "RandOM" };
            SimpleEntity expectedEntity2 = new() { FakeInt = 3, FakeString = "random" };
            SimpleEntity expectedEntity3 = new() { FakeInt = 4, FakeString = "RANdOM" };
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity);
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity1);
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity2);
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity3);
            _unitOfWork.SaveChanges();

            IEnumerable<string> searchedValue = new string[1] { expectedEntity.FakeString };

            // Act
            IEnumerable<SimpleEntity> simpleEntitiesFromDatabase = _unitOfWork
                .GetRepository<SimpleEntity>()
                .GetMany(s => s.Like(s.FakeString, "%D%", true));

            // Assert
            Assert.True(simpleEntitiesFromDatabase.Count() == searchedValue.Count());
            Assert.True(searchedValue.ToList().SequenceEqual(simpleEntitiesFromDatabase.Select(s => s.FakeString)));
        }

        [Fact]
        [Trait("SELECT", "MANY")]
        public void SelectALl_ValueWithDifferentCase_ReturnValuesWithCaseSensitiveNotTakenIntoAccount()
        {
            // Arrange
            SimpleEntity expectedEntity = new() { FakeInt = 1, FakeString = "RANDOM" };
            SimpleEntity expectedEntity1 = new() { FakeInt = 2, FakeString = "RandOM" };
            SimpleEntity expectedEntity2 = new() { FakeInt = 3, FakeString = "random" };
            SimpleEntity expectedEntity3 = new() { FakeInt = 4, FakeString = "RANdOM" };
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity);
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity1);
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity2);
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity3);
            _unitOfWork.SaveChanges();

            IEnumerable<string> searchedValue = new string[4] { expectedEntity.FakeString, expectedEntity1.FakeString, expectedEntity2.FakeString, expectedEntity3.FakeString };

            // Act
            IEnumerable<SimpleEntity> simpleEntitiesFromDatabase = _unitOfWork
                .GetRepository<SimpleEntity>()
                .GetMany(s => s.Like(s.FakeString, "%D%", false));

            // Assert
            Assert.True(simpleEntitiesFromDatabase.Count() == searchedValue.Count());
            Assert.True(searchedValue.ToList().SequenceEqual(simpleEntitiesFromDatabase.Select(s => s.FakeString)));
        }

        [Fact]
        [Trait("SELECT", "MANY")]
        public void SelectALl_ValueNotExisting_ReturnEmptyList()
        {
            // Arrange
            SimpleEntity expectedEntity = new() { FakeInt = 99, FakeString = "value" };
            SimpleEntity unexpectedEntity = new() { FakeInt = 0, FakeString = "value" };
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity);
            _unitOfWork.SaveChanges();

            // Act
            IEnumerable<SimpleEntity> simpleEntitiesFromDatabase = _unitOfWork
                .GetRepository<SimpleEntity>()
                .GetMany(s => s.FakeInt == unexpectedEntity.FakeInt);

            // Assert
            Assert.Empty(simpleEntitiesFromDatabase);
        }

        #endregion

        #region MAX

        [Fact]
        [Trait("MAX", "NORMAL USE")]
        public void SelectMax_WithValue_ReturnMaxValue()
        {
            // Arrange
            SimpleEntity expectedEntity = new() { FakeInt = 999, FakeString = "value" };
            _unitOfWork.GetRepository<SimpleEntity>().Insert(expectedEntity);
            _unitOfWork.SaveChanges();

            // Act
            int maxValueFromDatabase = _unitOfWork.GetRepository<SimpleEntity>().Max<int>(nameof(SimpleEntity.FakeInt));

            // Assert
            Assert.Equal(expectedEntity.FakeInt, maxValueFromDatabase);

        }

        #endregion

        #region DISPOSING METHODS

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _databaseHandler.Dispose();
                _unitOfWork.Dispose();
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}