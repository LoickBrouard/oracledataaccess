﻿using System;
using System.Linq;

using Domain.Core.Exceptions;
using Domain.Core.Interfaces;

using Infrastructure;
using Infrastructure.Exceptions;

using Test_Resources.Database;
using Test_Resources.FakeEntities;

using Xunit;

namespace Test_Infrastructure

{

    [Collection("Sequential")]
    [CollectionDefinition("Sequential", DisableParallelization = true)]
    public class StoredProcedureTests : IDisposable
    {
        private bool disposedValue;
        private readonly DatabaseHandler _databaseHandler;
        private readonly IUnitOfWork _unitOfWork;

        public StoredProcedureTests()
        {
            _databaseHandler = new DatabaseHandler(new Scheme[2] {
                StoredProcedure.GetScheme(),
                RefCursorStoredProcedure.GetScheme() 
            });
            _unitOfWork = new UnitOfWork(DatabaseHandler.CONNECTION_STRING);
        }

        [Fact]
        [Trait("STORED PROCEDURE", "CALL")]
        public void Storedprocedure_NormalUsage_ReturnEntity()
        {
            // Arrange
            StoredProcedureInput storedProcedureInput = new("fake", 1, new DateTime());

            // Act
            StoredProcedureOutput storedProcedureOutPut = _unitOfWork
                .GetStoredProcedure<StoredProcedureInput, StoredProcedureOutput>(StoredProcedure.NAME)
                .WithInput(storedProcedureInput)
                .Execute();

            // Assert
            Assert.Equal($"out{storedProcedureInput.InFakeString}", storedProcedureOutPut.OutfakeString);
            Assert.Equal(storedProcedureInput.InFakeInt + 100, storedProcedureOutPut.OutfakeInt);
            Assert.Equal(storedProcedureInput.InFakeDate.AddMinutes(1), storedProcedureOutPut.Outsysdate);
        }

        [Fact]
        [Trait("STORED PROCEDURE", "REF_CURSOR")]
        public void Storedprocedure_RefCursorAnonymousUsage_ReturnEntity()
        {
            // Arrange
            RefCursorStoredProcedureInput storedProcedureInput = new("fake", 1, new DateTime());

            // Act
            AnonymousRefCursorOutput storedProcedureOutPut = _unitOfWork
                .GetStoredProcedure<RefCursorStoredProcedureInput, AnonymousRefCursorOutput>(RefCursorStoredProcedure.NAME)
                .WithInput(storedProcedureInput)
                .ExecuteWithRefCursor<object>();

            // Assert
            Assert.Equal($"out{storedProcedureInput.InFakeString}", storedProcedureOutPut.OutfakeString);
            Assert.Equal(storedProcedureInput.InFakeInt + 100, storedProcedureOutPut.OutfakeInt);
            Assert.Equal(storedProcedureInput.InFakeDate.AddMinutes(1), storedProcedureOutPut.Outsysdate);
            Assert.NotNull(storedProcedureOutPut.OutCursor);
        }

        [Fact]
        [Trait("STORED PROCEDURE", "REF_CURSOR")]
        public void Storedprocedure_RefCursorNormalUsage_ReturnEntity()
        {
            // Arrange
            RefCursorStoredProcedureInput storedProcedureInput = new("fake", 1, new DateTime());

            // Act
            RefCursorOutput storedProcedureOutPut = _unitOfWork
                .GetStoredProcedure<RefCursorStoredProcedureInput, RefCursorOutput>(RefCursorStoredProcedure.NAME)
                .WithInput(storedProcedureInput)
                .ExecuteWithRefCursor<refCursorObject>();

            // Assert
            Assert.Equal($"out{storedProcedureInput.InFakeString}", storedProcedureOutPut.OutfakeString);
            Assert.Equal(storedProcedureInput.InFakeInt + 100, storedProcedureOutPut.OutfakeInt);
            Assert.Equal(storedProcedureInput.InFakeDate.AddMinutes(1), storedProcedureOutPut.Outsysdate);
            Assert.Equal(storedProcedureOutPut.OutCursor.Count(), 3);
        }

        [Fact]
        [Trait("STORED PROCEDURE", "ERROR")]
        public void Storedprocedure_WithNullInput_ThrowError()
        {
            // Arrange

            // Act
            Action act = new(() => _unitOfWork
                .GetStoredProcedure<StoredProcedureInput, StoredProcedureOutput>(StoredProcedure.NAME)
                .WithInput(null)
                .Execute());

            // Assert
            DatabaseException exception = Assert.Throws<DatabaseException>(act);
            Assert.IsType<InputNullException>(exception.InnerException);
        }

        #region DISPOSING METHODS

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }
                _databaseHandler.Dispose();
                _unitOfWork.Dispose();
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
