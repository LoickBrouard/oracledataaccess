﻿using System;
using System.Collections.Generic;
using System.Linq;

using Domain.Core.Interfaces;

using Infrastructure;

using Test_Resources.Database;
using Test_Resources.FakeEntities;

using Xunit;

namespace Test_Infrastructure;

[Collection("Sequential")]
[CollectionDefinition("Sequential", DisableParallelization = true)]
public class InsertDatabaseTests : IDisposable
{
    private bool disposedValue;
    private readonly DatabaseHandler _databaseHandler;
    private readonly IUnitOfWork _unitOfWork;

    public InsertDatabaseTests()
    {
        _databaseHandler = new DatabaseHandler(new Scheme[]{ 
            SimpleEntity.GetScheme(),
            AttributedEntity.GetScheme(),
        });
        _unitOfWork = new UnitOfWork(DatabaseHandler.CONNECTION_STRING);
    }

    #region INSERT MULTIPLE

    [Fact]
    [Trait("INSERT", "INSERT MULTIPLE")]
    public void Transaction_MultipleRowToInsert_InsertRow()
    {
        // Arrange
        SimpleEntity firstEntity = new() { FakeInt = 1, FakeString = "valueA" };
        SimpleEntity secondEntity = new() { FakeInt = 2, FakeString = "valueB" };
        ICollection<SimpleEntity> expectedEntites = _unitOfWork.GetRepository<SimpleEntity>().GetAll().ToList();
        expectedEntites.Add(firstEntity);
        expectedEntites.Add(secondEntity);


        // Act
        _unitOfWork.GetRepository<SimpleEntity>().Insert(firstEntity);
        _unitOfWork.GetRepository<SimpleEntity>().Insert(secondEntity);
        _unitOfWork.SaveChanges();

        // Assert
        IEnumerable<SimpleEntity> entitiesFromDatabase = _unitOfWork.GetRepository<SimpleEntity>().GetAll();
        Assert.Equal(new HashSet<SimpleEntity>(expectedEntites), new HashSet<SimpleEntity>(entitiesFromDatabase));
        // Note : HashSet is used to compare elements without any order consideration

    }

    [Fact]
    [Trait("INSERT", "INSERT MULTIPLE")]
    public void Transaction_MultipleRowToInsertWithOneRollBack_InsertRowNotRolledBack()
    {
        // Arrange
        SimpleEntity rolledBackEntity = new() { FakeInt = 1, FakeString = "valueA" };
        SimpleEntity savedEntity = new() { FakeInt = 2, FakeString = "valueB" };

        List<SimpleEntity> expectedEntites = _unitOfWork.GetRepository<SimpleEntity>().GetAll().ToList();
        expectedEntites.Add(savedEntity);

        // Act
        _unitOfWork.GetRepository<SimpleEntity>().Insert(rolledBackEntity);
        _unitOfWork.Rollback();
        _unitOfWork.GetRepository<SimpleEntity>().Insert(savedEntity);
        _unitOfWork.SaveChanges();
        List<SimpleEntity> entitiesFromDatabase = _unitOfWork.GetRepository<SimpleEntity>().GetAll().ToList();

        // Assert
        Assert.Equal(new HashSet<SimpleEntity>(expectedEntites), new HashSet<SimpleEntity>(entitiesFromDatabase));
        // Note : HashSet is used to compare elements without any order consideration

    }

    #endregion

    #region INSERT WITH RETURNING VALUE

    [Fact]
    [Trait("INSERT", "INSERT MULTIPLE")]
    public void Transaction_InsertAutoIncrementId_ReturnEntityWithIdAndDate()
    {
        // Arrange
        DateTime dt = _unitOfWork.GetDatabaseDateTime();
        AttributedEntity savedEntity = new() { FakeString = "valueB", FakeDifferentNameEntity = "something" };
        AttributedEntity expectedEntity = _unitOfWork.GetRepository<AttributedEntity>().GetAll()?.MaxBy(ae => ae.FakeInt) 
            ?? new() { FakeInt = 0, FakeString = "valueB", FakeDifferentNameEntity = "something" };

        // Act
        savedEntity = _unitOfWork.GetRepository<AttributedEntity>().Insert(savedEntity);
        _unitOfWork.SaveChanges();

        // Assert
        Assert.Equal(expectedEntity.FakeInt + 1, savedEntity.FakeInt);
        Assert.Equal(expectedEntity.FakeString, savedEntity.FakeString);
        Assert.Equal(expectedEntity.FakeDifferentNameEntity, savedEntity.FakeDifferentNameEntity);
        Assert.True(savedEntity.FakeDatetime >= dt);
    }

    #endregion

    #region SYSDATEONINSERT

    [Fact]
    [Trait("INSERT", "INSERT MULTIPLE")]
    public void Transaction_InsertSysdatePropertyWithExistingValue_ReturnSysdateInsteadOfValue()
    {
        // Arrange
        DateTime oldDateTime = new (1991,05,14); // C'est mon anniversaire :)
        AttributedEntity savedEntity = new() { FakeString = "valueB", FakeDatetime = oldDateTime };

        // Act
        savedEntity = _unitOfWork.GetRepository<AttributedEntity>().Insert(savedEntity);
        _unitOfWork.SaveChanges();

        // Assert
        Assert.True(savedEntity.FakeDatetime >= oldDateTime);
        Assert.NotEqual(oldDateTime, savedEntity.FakeDatetime);
    }

    #endregion

    #region DISPOSING METHODS

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            _databaseHandler.Dispose();
            _unitOfWork.Dispose();
            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    #endregion
}
