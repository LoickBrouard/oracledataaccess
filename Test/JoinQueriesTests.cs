﻿using System;
using System.Linq.Expressions;

using Domain.Core.Primitives;

using OracleSqlBuilder;
using OracleSqlBuilder.Exceptions;
using OracleSqlBuilder.Exceptions.LambdaVisitorExceptions;
using OracleSqlBuilder.Models;

using Test_Resources.FakeEntities;

using Xunit;

namespace Test_SqlBuilder
{
    public class JoinQueriesTests
    {
        private readonly string ExpectedSelect;
        private readonly string ExpectedFromExpression;
        private readonly string ExpectedJoinExpression;

        public JoinQueriesTests()
        {
            ExpectedSelect = "SELECT foreign_entity.navigation_entity_id AS NavigationEntityId,foreign_entity.fake_foreign_int AS FakeForeignInt,navigation_entity.fake_id AS FakeId";
            ExpectedFromExpression = "FROM foreign_entity";
            ExpectedJoinExpression = "JOIN navigation_entity ON foreign_entity.navigation_entity_id = navigation_entity.fake_id";
        }

        #region NORMAL USE

        [Fact]
        [Trait("JOIN", "NORMAL USE")]
        public void SelectJoin_RightJoin_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<ForeignEntity>
                .SelectJoin<NavigationEntity>()
                .Join<NavigationEntity>(Join.Right, (a, b) => a.NavigationEntityId == b.FakeId )
                .Build();

            // Assert
            Assert.Equal($"{ExpectedSelect} {ExpectedFromExpression} Right {ExpectedJoinExpression}", query.QueryString);
        }

        [Fact]
        [Trait("JOIN", "NORMAL USE")]
        public void SelectJoin_LeftJoin_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<ForeignEntity>
                .SelectJoin<NavigationEntity>()
                .Join<NavigationEntity>(Join.Left, (a, b) => a.NavigationEntityId == b.FakeId)
                .Build();

            // Assert
            Assert.Equal($"{ExpectedSelect} {ExpectedFromExpression} Left {ExpectedJoinExpression}", query.QueryString);
        }

        [Fact]
        [Trait("JOIN", "NORMAL USE")]
        public void SelectJoin_FullJoin_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<ForeignEntity>
                .SelectJoin<NavigationEntity>()
                .Join<NavigationEntity>(Join.Full, (a, b) => a.NavigationEntityId == b.FakeId)
                .Build();

            // Assert
            Assert.Equal($"{ExpectedSelect} {ExpectedFromExpression} Full {ExpectedJoinExpression}", query.QueryString);
        }

        [Fact]
        [Trait("JOIN", "NORMAL USE")]
        public void SelectJoin_InnerJoin_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<ForeignEntity>
                .SelectJoin<NavigationEntity>()
                .Join(Join.Inner, (ForeignEntity a, NavigationEntity b) => a.NavigationEntityId == b.FakeId)
                .Build();

            // Assert
            Assert.Equal($"{ExpectedSelect} {ExpectedFromExpression} Inner {ExpectedJoinExpression}", query.QueryString);
        }

        #endregion

        #region ERRORS

        [Fact]
        [Trait("JOIN", "ERROR")]
        public void SelectJoin_InnerJoinWithoutUnaryExpression_ThrowError()
        {
            // Arrange
            Expression<Func<ForeignEntity, NavigationEntity, bool>> joinExpression = (foreignEntity, navigationEntity) => true;

            // Act
            Action act = new (()=>OracleQueryBuilder<ForeignEntity>
                .SelectJoin<NavigationEntity>()
                .Join(Join.Inner, joinExpression)
                .Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<JoinLambdaVisitorException>(exception.InnerException);
        }

        [Fact]
        [Trait("JOIN", "ERROR")]
        public void SelectJoin_EntitiesPropertyMissing_ThrowError()
        {
            // Arrange
            int notAnEntityProperty = 1;
            Expression<Func<ForeignEntity, NavigationEntity, bool>> joinExpression = (foreignEntity, navigationEntity) => notAnEntityProperty == notAnEntityProperty;

            // Act
            Action act = new(() => OracleQueryBuilder<ForeignEntity>
                .SelectJoin<NavigationEntity>()
                .Join(Join.Inner, joinExpression)
                .Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<JoinLambdaVisitorException>(exception.InnerException);
        }


        [Fact]
        [Trait("JOIN", "ERROR")]
        public void SelectJoin_LeftEntityPropertyMissing_ThrowError()
        {   
            // Arrange
            int notAnEntityProperty = 1;
            Expression<Func<ForeignEntity, NavigationEntity, bool>> joinExpression = (foreignEntity, navigationEntity) => notAnEntityProperty == navigationEntity.FakeId;

            // Act
            Action act = new(() => OracleQueryBuilder<ForeignEntity>
                .SelectJoin<NavigationEntity>()
                .Join(Join.Inner, joinExpression)
                .Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<JoinLambdaVisitorException>(exception.InnerException);
        }

        [Fact]
        [Trait("JOIN", "ERROR")]
        public void SelectJoin_PropertyWithNoName_ThrowError()
        {
            // Arrange
            Expression<Func<ForeignEntity, NavigationEntity, bool>> joinExpression = (foreignEntity, navigationEntity) => 0 == navigationEntity.FakeId;

            // Act
            Action act = new(() => OracleQueryBuilder<ForeignEntity>
                .SelectJoin<NavigationEntity>()
                .Join(Join.Inner, joinExpression)
                .Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<JoinLambdaVisitorException>(exception.InnerException);
        }

        [Fact]
        [Trait("JOIN", "ERROR")]
        public void SelectJoin_RightEntityPropertyMissing_ThrowError()
        {
            // Arrange
            int notAnEntityProperty = 1;
            Expression<Func<ForeignEntity, NavigationEntity, bool>> joinExpression = (foreignEntity, navigationEntity) => foreignEntity.NavigationEntityId == notAnEntityProperty;

            // Act
            Action act = new(() => OracleQueryBuilder<ForeignEntity>
                .SelectJoin<NavigationEntity>()
                .Join(Join.Inner, joinExpression)
                .Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<JoinLambdaVisitorException>(exception.InnerException);
        }

        #endregion
    }
}
