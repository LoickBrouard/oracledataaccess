﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Domain.Core.Primitives;

using OracleSqlBuilder;
using OracleSqlBuilder.Exceptions;
using OracleSqlBuilder.Exceptions.LambdaVisitorExceptions;
using OracleSqlBuilder.Helpers;
using OracleSqlBuilder.Models;

using Test_Resources.FakeEntities;
using Test_Resources.Helpers;

using Xunit;

namespace Test_SqlBuilder
{
    public class WhereQueriesTests
    {
        #region WHERE COMPARISON

        [Fact]
        [Trait("WHERE", "COMPARISON")]
        public void SelectWhere_EqualCondition_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeString == "A";

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("simple_entity.fake_string=:FakeString"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "COMPARISON")]
        public void SelectWhere_DifferentCondition_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeString != "A";

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("simple_entity.fake_string!=:FakeString"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "COMPARISON")]
        public void SelectWhere_NullCondition_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeString == null;

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("simple_entity.fake_string IS NULL"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "COMPARISON")]
        public void SelectWhere_GreaterThanCondition_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeInt > 0;

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("simple_entity.fake_int>:FakeInt"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "COMPARISON")]
        public void SelectWhere_LowerThanCondition_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeInt < 0;

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("simple_entity.fake_int<:FakeInt"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "COMPARISON")]
        public void SelectWhere_GreaterOrEqualThanCondition_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeInt >= 0;

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("simple_entity.fake_int>=:FakeInt"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "COMPARISON")]
        public void SelectWhere_LowerOrEqualThanCondition_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeInt <= 0;

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("simple_entity.fake_int<=:FakeInt"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "COMPARISON")]
        public void SelectWhere_LowerOrEqualThanCondition_WhereFilterContainsValues()
        {
            // Arrange
            int FakeIntCondition = 0;
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeInt <= FakeIntCondition;

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
            Assert.Equal(FakeIntCondition, query.WhereFilter["FakeInt"]);
        }

        [Fact]
        [Trait("WHERE", "COMPARISON")]
        public void SelectWhere_UnhandledMethodCondition_ThrowError()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeInt.GetType() == typeof(int);

            // Act
            Action act = new(() => OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<MethodCallUnknownException>(exception.InnerException);
        }

        [Fact]
        [Trait("WHERE", "COMPARISON")]
        public void SelectWhere_UnhandledCondition_ThrowError()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeInt % 2 == 2;

            // Act
            Action act = new(() => OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LogicalOperatorUnknownException>(exception.InnerException);
        }

        #endregion

        #region WHERE MANY CONDITIONS

        [Fact]
        [Trait("WHERE", " MANY CONDITIONS")]
        public void SelectWhereManyConditions_AndAlso_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeString == "A" && a.FakeInt < 0;

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("(simple_entity.fake_string=:FakeString)and(simple_entity.fake_int<:FakeInt)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", " MANY CONDITIONS")]
        public void SelectWhereManyConditions_And_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeString == "A" & a.FakeInt < 0;

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("(simple_entity.fake_string=:FakeString)and(simple_entity.fake_int<:FakeInt)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", " MANY CONDITIONS")]
        public void SelectWhereManyConditions_AndSameCondition_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeString != "A" & a.FakeString != "B";

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("(simple_entity.fake_string!=:FakeString)and(simple_entity.fake_string!=:FakeStringA)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", " MANY CONDITIONS")]
        public void SelectWhereManyConditions_OrAlso_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeString == "A" || a.FakeInt < 0;

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("(simple_entity.fake_string=:FakeString)or(simple_entity.fake_int<:FakeInt)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", " MANY CONDITIONS")]
        public void SelectWhereManyConditions_Or_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeString == "A" | a.FakeInt < 0;

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("(simple_entity.fake_string=:FakeString)or(simple_entity.fake_int<:FakeInt)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", " MANY CONDITIONS")]
        public void SelectWhereManyConditions_OrSameCondition_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeString != "A" | a.FakeString != "B";

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("(simple_entity.fake_string!=:FakeString)or(simple_entity.fake_string!=:FakeStringA)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", " MANY CONDITIONS")]
        public void SelectWhereManyConditions_OrSameCondition_WhereFilterContainsValues()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => a.FakeString != "A" | a.FakeString != "B";

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(2, query.WhereFilter.Keys?.Count);
            Assert.Equal("A", query.WhereFilter["FakeString"]);
            Assert.Equal("B", query.WhereFilter["FakeStringA"]);
        }

        [Fact]
        [Trait("WHERE", " MANY CONDITIONS")]
        public void SelectWhereManyConditions_ImbricateConditions_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => (a.FakeString == "A" && a.FakeInt == 2) | a.FakeString != "B";

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("((simple_entity.fake_string=:FakeString)and(simple_entity.fake_int=:FakeInt))or(simple_entity.fake_string!=:FakeStringA)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", " MANY CONDITIONS")]
        public void SelectWhereManyConditions_ManyTimeSameField_WhereFilterContainsValues()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => (
            a.FakeString == "Z" || a.FakeString != "E" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" ||a.FakeString != "Z" ||a.FakeString != "c");

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(28, query.WhereFilter.Keys?.Count);
            Assert.Equal("Z", query.WhereFilter["FakeString"]);
            Assert.Equal("E", query.WhereFilter["FakeStringA"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringB"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringC"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringD"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringE"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringF"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringG"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringH"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringI"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringJ"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringK"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringL"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringM"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringN"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringO"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringP"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringQ"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringR"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringS"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringT"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringU"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringV"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringW"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringX"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringY"]);
            Assert.Equal("Z", query.WhereFilter["FakeStringZ"]);
            Assert.Equal("c", query.WhereFilter["FakeStringAA"]);
        }
        
        [Fact]
        [Trait("WHERE", " MANY CONDITIONS")]
        public void SelectWhereManyConditions_ManyTimeSameField_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<SimpleEntity, bool>> predicate = (a) => (
            a.FakeString == "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" || a.FakeString != "Z" ||a.FakeString != "Z" ||a.FakeString != "Z");

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectWhereSqlString("(((((((((((((((((((((((((((simple_entity.fake_string=:FakeString)or(simple_entity.fake_string!=:FakeStringA))or(simple_entity.fake_string!=:FakeStringB))or(simple_entity.fake_string!=:FakeStringC))or(simple_entity.fake_string!=:FakeStringD))or(simple_entity.fake_string!=:FakeStringE))or(simple_entity.fake_string!=:FakeStringF))or(simple_entity.fake_string!=:FakeStringG))or(simple_entity.fake_string!=:FakeStringH))or(simple_entity.fake_string!=:FakeStringI))or(simple_entity.fake_string!=:FakeStringJ))or(simple_entity.fake_string!=:FakeStringK))or(simple_entity.fake_string!=:FakeStringL))or(simple_entity.fake_string!=:FakeStringM))or(simple_entity.fake_string!=:FakeStringN))or(simple_entity.fake_string!=:FakeStringO))or(simple_entity.fake_string!=:FakeStringP))or(simple_entity.fake_string!=:FakeStringQ))or(simple_entity.fake_string!=:FakeStringR))or(simple_entity.fake_string!=:FakeStringS))or(simple_entity.fake_string!=:FakeStringT))or(simple_entity.fake_string!=:FakeStringU))or(simple_entity.fake_string!=:FakeStringV))or(simple_entity.fake_string!=:FakeStringW))or(simple_entity.fake_string!=:FakeStringX))or(simple_entity.fake_string!=:FakeStringY))or(simple_entity.fake_string!=:FakeStringZ))or(simple_entity.fake_string!=:FakeStringAA)"), query.QueryString);
        }

        #endregion

        #region WHERE BOOLEAN

        [Fact]
        [Trait("WHERE", "BOOLEAN")]
        public void SelectWhereBoolean_TrueParameter_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.FakeBool == true;

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_bool=:FakeBool"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "BOOLEAN")]
        public void SelectWhereBoolean_FalseParameter_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.FakeBool == false;

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_bool=:FakeBool"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "BOOLEAN")]
        public void SelectWhereBoolean_FalseParameter_WhereFilterContainsValues()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.FakeBool == false;

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(false, query.WhereFilter["FakeBool"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        #endregion

        #region WHERE DATETIME

        [Fact]
        [Trait("WHERE", "DATETIME")]
        public void SelectWhereDatetime_NewDatetime_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.FakeDatetime < new DateTime(1,2,3);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_datetime<:FakeDatetime"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "DATETIME")]
        public void SelectWhereDatetime_VariableDatetime_GenerateOracleQuery()
        {
            // Arrange
            DateTime expectedDateTime = new (1, 2, 3);
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.FakeDatetime < expectedDateTime;

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_datetime<:FakeDatetime"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "DATETIME")]
        public void SelectWhereDatetime_DatetimeNow_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.FakeDatetime < DateTime.Now;

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();
            
            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_datetime<:FakeDatetime"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE", "DATETIME")]
        public void SelectWhereDatetime_DatetimeNow_WhereFilterContainsValues()
        {
            // Arrange
            DateTime datetime = DateTime.Now;
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.FakeDatetime < datetime;

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(datetime, query.WhereFilter["FakeDatetime"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE", "DATETIME")]
        public void SelectWhereDatetime_VariableDatetime_WhereFilterContainsValues()
        {
            // Arrange
            DateTime expectedDateTime = new (1, 2, 3);
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.FakeDatetime < expectedDateTime;

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(expectedDateTime, query.WhereFilter["FakeDatetime"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE", "DATETIME")]
        public void SelectWhereDatetime_FalseParameter_WhereFilterContainsValues()
        {
            // Arrange
            DateTime expectedDateTime= new DateTime(1, 2, 3);
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.FakeDatetime < new DateTime(1, 2, 3);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();


            // Assert
            Assert.Equal(expectedDateTime, query.WhereFilter["FakeDatetime"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }


        #endregion

        #region WHERE NULL

        [Fact]
        [Trait("WHERE", "NULL")]
        public void SelectWhereNull_Null_ThrowError()
        {
            // Arrange

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(null).Build());

            // Assert
            Assert.Throws<SqlBuilderException>(act);
        }

        #endregion

        #region WHERE LIKE

        #region VARIABLE

        [Fact]
        [Trait("WHERE LIKE", "VARIABLE")]
        public void SelectWhereLike_LikeVariableValue_GenerateOracleQuery()
        {
            // Arrange
            string likeValue = "%value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, likeValue, false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) LIKE LOWER('%' || :likeParameter)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE LIKE", "VARIABLE")]
        public void SelectWhereLike_LikeVariableValue_WhereFilterContainsValues()
        {
            // Arrange
            string likeValue = "%value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, likeValue, false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal("value", query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE LIKE", "VARIABLE")]
        public void SelectWhereLike_LikeSemiVariableValue_GenerateOracleQuery()
        {
            // Arrange
            string likeValue = "value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, $"%{likeValue}", false);


            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) LIKE LOWER('%' || :likeParameter)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE LIKE", "VARIABLE")]
        public void SelectWhereLike_LikeSemiVariableValue_WhereFilterContainsValues()
        {
            // Arrange
            string likeValue = "value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, $"%{likeValue}", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Adssert
            Assert.Equal("value", query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE LIKE", "VARIABLE")]
        public void SelectWhereLike_LikeOperationVariableValue_GenerateOracleQuery()
        {
            // Arrange
            string likeValue = "value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, "%" + likeValue, false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) LIKE LOWER('%' || :likeParameter)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE LIKE", "VARIABLE")]
        public void SelectWhereLike_LikeOperationVariableValue_WhereFilterContainsValues()
        {
            // Arrange
            string likeValue = "value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, "%" + likeValue, false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal("value", query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE LIKE", "VARIABLE")]
        public void SelectWhereLike_DirectValue_WhereFilterContainsValues()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, "%value", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal("value", query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        #endregion

        #region NORMAL USE

        [Fact]
        [Trait("WHERE LIKE", "NORMAL USE")]
        public void SelectWhereLike_SimpleLikeLeft_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, "%value", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) LIKE LOWER('%' || :likeParameter)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE LIKE", "NORMAL USE")]
        public void SelectWhereLike_SimpleLikeRight_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, "value%", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) LIKE LOWER(:likeParameter || '%')"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE LIKE", "NORMAL USE")]
        public void SelectWhereLike_SimpleLikeLeftRight_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, "%value%", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) LIKE LOWER('%' || :likeParameter || '%')"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE LIKE", "NORMAL USE")]
        public void SelectWhereLike_MultipleLikeStatement_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, "%value%", false) && a.NotLike(a.FakeString, "lue%", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("(LOWER(complex_entity.fake_string) LIKE LOWER('%' || :likeParameter || '%'))and(LOWER(complex_entity.fake_string) NOT LIKE LOWER(:likeParameterA || '%'))"), query.QueryString);
        }

        #endregion

        #region ERRORS

        [Fact]
        [Trait("WHERE LIKE", "ERRORS")]
        public void SelectWhereLike_LikeLeftRightWithNoValue_ThrowError()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, "%%", false);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }

        [Fact]
        [Trait("WHERE LIKE", "ERRORS")]
        public void SelectWhereLike_WithNoPercentageSymbol_ThrowError()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, "value", false);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }


        [Fact]
        [Trait("WHERE LIKE", "ERRORS")]
        public void SelectWhereLike_WithNullValue_ThrowError()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, null, false);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }

        [Fact]
        [Trait("WHERE LIKE", "ODD TYPES")]
        public void SelectWhereLike_LikeWithOdType_ThrowException()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeInt, "%value", true);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }

        #endregion

        #region CASE SENSITIVE

        [Fact]
        [Trait("WHERE LIKE", "CASE_SENSITIVE")]
        public void SelectWhereLike_LikeCaseSensitive_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, "%value", true);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_string LIKE '%' || :likeParameter"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE LIKE", "CASE_SENSITIVE")]
        public void SelectWhereLike_LikeCaseSensitive_WhereFilterContainsValues()
        {
            // Arrange
            string value = "vAlUe";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, $"%{value}", true);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(value, query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE LIKE", "CASE_SENSITIVE")]
        public void SelectWhereLike_LikeCaseInsensitive_GenerateOracleQuery()
        {
            // Arrange
            string value = "vAlUe";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, $"%{value}", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) LIKE LOWER('%' || :likeParameter)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE LIKE", "CASE_SENSITIVE")]
        public void SelectWhereLike_LikeCaseInsensitive_WhereFilterContainsValues()
        {
            // Arrange
            string value = "vAlUe";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.Like(a.FakeString, $"%{value}", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(value.ToLower(), query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        #endregion

        #endregion

        #region WHERE NOT LIKE

        #region VARIABLE

        [Fact]
        [Trait("WHERE NOT LIKE", "VARIABLE")]
        public void SelectWhereNotLike_LikeVariableValue_GenerateOracleQuery()
        {
            // Arrange
            string likeValue = "%value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, likeValue, false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) NOT LIKE LOWER('%' || :likeParameter)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "VARIABLE")]
        public void SelectWhereNotLike_LikeVariableValue_WhereFilterContainsValues()
        {
            // Arrange
            string likeValue = "%value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, likeValue, false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal("value", query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "VARIABLE")]
        public void SelectWhereNotLike_LikeSemiVariableValue_GenerateOracleQuery()
        {
            // Arrange
            string likeValue = "value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, $"%{likeValue}", false);


            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) NOT LIKE LOWER('%' || :likeParameter)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "VARIABLE")]
        public void SelectWhereNotLike_LikeSemiVariableValue_WhereFilterContainsValues()
        {
            // Arrange
            string likeValue = "value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, $"%{likeValue}", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Adssert
            Assert.Equal("value", query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "VARIABLE")]
        public void SelectWhereNotLike_LikeOperationVariableValue_GenerateOracleQuery()
        {
            // Arrange
            string likeValue = "value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, "%" + likeValue, false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) NOT LIKE LOWER('%' || :likeParameter)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "VARIABLE")]
        public void SelectWhereNotLike_LikeOperationVariableValue_WhereFilterContainsValues()
        {
            // Arrange
            string likeValue = "value";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, "%" + likeValue, false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal("value", query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "VARIABLE")]
        public void SelectWhereNotLike_DirectValue_WhereFilterContainsValues()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, "%value", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal("value", query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        #endregion

        #region NORMAL USE

        [Fact]
        [Trait("WHERE NOT LIKE", "NORMAL USE")]
        public void SelectWhereNotLike_SimpleLikeLeft_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, "%value", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) NOT LIKE LOWER('%' || :likeParameter)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE LIKE", "NORMAL USE")]
        public void SelectWhereNotLike_SimpleLikeRight_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, "value%", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) NOT LIKE LOWER(:likeParameter || '%')"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "NORMAL USE")]
        public void SelectWhereNotLike_SimpleLikeLeftRight_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, "%value%", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("LOWER(complex_entity.fake_string) NOT LIKE LOWER('%' || :likeParameter || '%')"), query.QueryString);
        }

        #endregion

        #region ERRORS

        [Fact]
        [Trait("WHERE NOT LIKE", "ERRORS")]
        public void SelectWhereNotLike_LikeLeftRightWithNoValue_ThrowError()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, "%%", false);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "ERRORS")]
        public void SelectWhereNotLike_WithNoPercentageSymbol_ThrowError()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, "value", false);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }


        [Fact]
        [Trait("WHERE NOT LIKE", "ERRORS")]
        public void SelectWhereNotLike_WithNullValue_ThrowError()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, null, false);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "ODD TYPES")]
        public void SelectWhereNotLike_LikeWithOdType_ThrowException()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeInt, "%value", true);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }

        #endregion

        #region CASE SENSITIVE

        [Fact]
        [Trait("WHERE NOT LIKE", "CASE_SENSITIVE")]
        public void SelectWhereNotLike_LikeCaseSensitive_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, "%value", true);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_string NOT LIKE '%' || :likeParameter"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "CASE_SENSITIVE")]
        public void SelectWhereNotLike_LikeCaseSensitive_WhereFilterContainsValues()
        {
            // Arrange
            string value = "vAlUe";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, $"%{value}", true);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(value, query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE NOT LIKE", "CASE_SENSITIVE")]
        public void SelectWhereNotLike_LikeCaseInsensitive_WhereFilterContainsValues()
        {
            // Arrange
            string value = "vAlUe";
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.NotLike(a.FakeString, $"%{value}", false);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(value.ToLower(), query.WhereFilter["likeParameter"]);
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
        }

        #endregion


        #endregion

        #region WHERE IN

        #region VARIABLES

        [Fact]
        [Trait("WHERE IN", "VARIABLES")]
        public void SelectWhereIn_ExistingVariable_GenerateOracleQuery()
        {
            // Arrange
            IEnumerable<string> whereInCondition = new List<string>() { "a", "b", "c" };
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, whereInCondition);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_string IN (:InParameter,:InParameterA,:InParameterAB)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE IN", "VARIABLES")]
        public void SelectWhereIn_ExistingVariable_WhereFilterContainsValues()
        {
            // Arrange
            string[] whereInCondition = new string[3] { "a", "b", "c" };
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, whereInCondition);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(whereInCondition[0], query.WhereFilter["InParameter"]);
            Assert.Equal(whereInCondition[1], query.WhereFilter["InParameterA"]);
            Assert.Equal(whereInCondition[2], query.WhereFilter["InParameterAB"]);
            Assert.Equal(whereInCondition.Length, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE IN", "VARIABLES")]
        public void SelectWhereIn_InlineVariable_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, new string[3]{ "a", "b", "c" });

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_string IN (:InParameter,:InParameterA,:InParameterAB)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE IN", "VARIABLES")]
        public void SelectWhereIn_InlineVariable_WhereFilterContainsValues()
        {
            // Arrange
            string[] whereInCondition = new string[3] { "a", "b", "c" };
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, whereInCondition);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(whereInCondition[0], query.WhereFilter["InParameter"]);
            Assert.Equal(whereInCondition[1], query.WhereFilter["InParameterA"]);
            Assert.Equal(whereInCondition[2], query.WhereFilter["InParameterAB"]);
            Assert.Equal(whereInCondition.Length, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE IN", "VARIABLES")]
        public void SelectWhereIn_StringVariable_GenerateOracleQuery()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeInt, new int[3] { 1, 2, 3 });

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_int IN (:InParameter,:InParameterA,:InParameterAB)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE IN", "VARIABLES")]
        public void SelectWhereIn_StringVariable_WhereFilterContainsValues()
        {
            // Arrange
            int[] whereInCondition = new int[3] { 1, 2, 3 };
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeInt, whereInCondition);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(whereInCondition[0], query.WhereFilter["InParameter"]);
            Assert.Equal(whereInCondition[1], query.WhereFilter["InParameterA"]);
            Assert.Equal(whereInCondition[2], query.WhereFilter["InParameterAB"]);
            Assert.Equal(whereInCondition.Length, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE IN", "VARIABLES")]
        public void SelectWhereIn_DateTimeVariable_GenerateOracleQuery()
        {
            // Arrange
            
            DateTime[] whereInCondition = new DateTime[2] { new DateTime(2023, 12, 2), new DateTime(2023, 12, 3) };
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeDatetime, whereInCondition);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_datetime IN (:InParameter,:InParameterA)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE IN", "VARIABLES")]
        public void SelectWhereIn_DatetimeVariable_WhereFilterContainsValues()
        {
            // Arrange
            DateTime[] whereInCondition = new DateTime[2] { new DateTime(2023, 12, 2), new DateTime(2023, 12, 3) };
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeDatetime, whereInCondition);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(whereInCondition[0], query.WhereFilter["InParameter"]);
            Assert.Equal(whereInCondition[1], query.WhereFilter["InParameterA"]);
            Assert.Equal(whereInCondition.Length, query.WhereFilter.Keys?.Count);
        }

        #endregion

        #region ARRAY TYPES

        [Fact]
        [Trait("WHERE IN", "ARRAY TYPES")]
        public void SelectWhereIn_InListValue_GenerateOracleQuery()
        {
            // Arrange
            IEnumerable<string > whereInCondition = new List<string>() { "a", "b", "c"};
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, whereInCondition);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_string IN (:InParameter,:InParameterA,:InParameterAB)"), query.QueryString);
        }

        [Fact]
        [Trait("WHERE IN", "ARRAY TYPES")]
        public void SelectWhereIn_InArrayValue_GenerateOracleQuery()
        {
            // Arrange
            string[] whereInCondition = new string[3] { "a", "b", "c" };
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, whereInCondition);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectWhereSqlString("complex_entity.fake_string IN (:InParameter,:InParameterA,:InParameterAB)"), query.QueryString);
        }

        #endregion

        #region ERRORS

        [Fact]
        [Trait("WHERE IN", "ERRORS")]
        public void SelectWhereIn_InWithEmptyList_ThrowError()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, new List<string>());

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }

        [Fact]
        [Trait("WHERE IN", "ERRORS")]
        public void SelectWhereIn_NullableList_ThrowError()
        {
            // Arrange
            List<string>? nullableList = null;
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, nullableList);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }

        [Fact]
        [Trait("WHERE IN", "ERRORS")]
        public void SelectWhereIn_EmptyList_ThrowError()
        {
            // Arrange
            List<string> nullableList = new ();
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, nullableList);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
            Assert.Equal("The list must not be empty", exception.InnerException?.Message);
        }

        [Fact]
        [Trait("WHERE IN", "ERRORS")]
        public void SelectWhereIn_NullList_ThrowError()
        {
            // Arrange
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, null);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
        }

        [Fact]
        [Trait("WHERE IN", "ERRORS")]
        public void SelectWhereIn_LongList_NotThrowError()
        {
            // Arrange
            string[] longList = ResourcesHelper.GenerateList<string>(OracleConfiguration.WHERE_IN_LIMIT);
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, longList);

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build();

            // Assert
            Assert.Equal(longList.Length, query.WhereFilter.Keys?.Count);
        }

        [Fact]
        [Trait("WHERE IN", "ERRORS")]
        public void SelectWhereIn_LongList_ThrowError()
        {
            // Arrange
            string[] longList = ResourcesHelper.GenerateList<string>(OracleConfiguration.WHERE_IN_LIMIT + 1);
            Expression<Func<ComplexEntity, bool>> predicate = (a) => a.In(a.FakeString, longList);

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().Where(predicate).Build());

            // Assert
            SqlBuilderException exception = Assert.Throws<SqlBuilderException>(act);
            Assert.IsType<LambdaVisitorException>(exception.InnerException);
            Assert.Equal($"The WHERE IN condition cannot be used with an array of more than {OracleConfiguration.WHERE_IN_LIMIT} elements", exception.InnerException?.Message);
        }

        #endregion

        #endregion
    }
}
