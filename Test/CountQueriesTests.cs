﻿using OracleSqlBuilder;
using OracleSqlBuilder.Models;

using Test_Resources.FakeEntities;

using Xunit;

namespace Test_SqlBuilder
{
    public class CountQueriesTests
    {
        #region MAX NORMAL USE

        [Fact]
        [Trait("COUNT", "NORMAL USE")]
        public void SelectCount_CountAllEntries_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Count().Build();
            // Assert
            Assert.Equal("SELECT COUNT(*) FROM simple_entity", query.QueryString);
        }

        #endregion

    }
}
