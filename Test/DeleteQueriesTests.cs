﻿using OracleSqlBuilder.Models;
using OracleSqlBuilder;
using Test_Resources.FakeEntities;
using Xunit;

namespace Test_SqlBuilder
{
    public class DeleteQueriesTests
    {
        #region NORMAL USE

        [Fact]
        [Trait("DELETE", "NORMAL USE")]
        public void Delete_SimpleEntity_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Delete().WhereId("a").Build();
            // Assert
            Assert.Equal(ComplexEntity.GetDeleteSqlQuery("complex_entity.fake_int=:FakeInt"), query.QueryString);
        }

        [Fact]
        [Trait("DELETE", "NORMAL USE")]
        public void Delete_AttributedEntity_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<AttributedEntity>.Delete().WhereId("a").Build();
            // Assert
            Assert.Equal(AttributedEntity.GetDeleteSqlQuery("other_TableName.fake_int=:FakeInt"), query.QueryString);
        }

        #endregion
    }
}
