﻿using OracleSqlBuilder.Models;
using OracleSqlBuilder;
using Test_Resources.FakeEntities;
using Xunit;

namespace Test_SqlBuilder
{
    public class InsertQueriesTests
    {

        #region NORMAL USE

        [Fact]
        [Trait("INSERT", "NORMAL USE")]
        public void Insert_SimpleEntity_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Add().Build();
            // Assert
            Assert.Equal(SimpleEntity.GetInsertSqlQuery(), query.QueryString);
        }

        [Fact]
        [Trait("INSERT", "NORMAL USE")]
        public void Insert_EntityWithVirtualProperty_GenerateOracleQueryWithoutVirtualProperties()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Add().Build();
            // Assert
            Assert.Equal(ComplexEntity.GetInsertSqlQuery(), query.QueryString);
        }
        
        [Fact]
        [Trait("INSERT", "NORMAL USE")]
        public void Insert_EntityWithAttributes_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<AttributedEntity>.Add().Build();
            // Assert
            Assert.Equal(AttributedEntity.GetInsertSqlQuery(), query.QueryString);
        }

        [Fact]
        [Trait("INSERT", "NORMAL USE")]
        public void Insert_AskForReturningAutoInsertedValues_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<AttributedEntity>.Add().ReturnElementsInsertedAutomatically().Build();
            // Assert
            Assert.Equal(AttributedEntity.GetInsertWithReturnStatementSqlQuery(), query.QueryString);
        }

        #endregion
    }
}
