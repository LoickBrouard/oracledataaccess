﻿using OracleSqlBuilder.Models;
using OracleSqlBuilder;
using Test_Resources.FakeEntities;
using Xunit;
using System;
using OracleSqlBuilder.Exceptions;

namespace Test_SqlBuilder
{
    public class UpdateQueriesTests
    {
        #region NORMAL USE

        [Fact]
        [Trait("UPDATE", "NORMAL USE")]
        public void Update_SimpleEntity_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Update().WhereId("a").Build();
            // Assert
            Assert.Equal(ComplexEntity.GetUpdateSqlQuery("complex_entity.fake_int=:FakeInt"), query.QueryString);
        }

        [Fact]
        [Trait("UPDATE", "NORMAL USE")]
        public void Update_AttributedEntity_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<AttributedEntity>.Update().WhereId("a").Build();
            // Assert
            Assert.Equal(AttributedEntity.GetUpdateSqlQuery("other_TableName.fake_int=:FakeInt"), query.QueryString);
        }

        #endregion

        #region ERRORS

        [Fact]
        [Trait("UPDATE", "ERRORS")]
        public void Update_EntityWithoutPrimaryKey_ThrowError()
        {
            // Arrange

            // Act
            Action act = new (()=>OracleQueryBuilder<SimpleEntity>.Update().WhereId("a").Build());
            // Assert
            Assert.Throws<PrimaryKeyAttributeMissingException>(act);
        }

        #endregion
    }
}
