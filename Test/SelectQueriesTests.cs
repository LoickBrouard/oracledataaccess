using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

using Domain.Core.Primitives;

using OracleSqlBuilder;
using OracleSqlBuilder.Exceptions;
using OracleSqlBuilder.Exceptions.LambdaVisitorExceptions;
using OracleSqlBuilder.Models;

using Test_Resources.FakeEntities;

using Xunit;

namespace Test_SqlBuilder
{
    public class SelectQueriesTests
    {

        public SelectQueriesTests()
        {
            // Setup
        }

        #region SELECT *

        [Fact]
        [Trait("SELECT", "*")]
        public void GenerateSelectOracleQuery_Entity_GenerateOracleQuery()
        {
            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Select().Build();

            // Assert
            Assert.Equal(SimpleEntity.GetSelectSqlString(), query.QueryString);
        }

        [Fact]
        [Trait("SELECT", "*")]
        public void GenerateSelectOracleQuery_EntityWithVirtualProperty_GenerateOracleQueryWithoutVirtualProperty()
        {
            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectSqlString(), query.QueryString);
        }

        [Fact]
        [Trait("SELECT", "*")]
        public void GenerateSelectOracleQuery_EmptyEntity_ThrowError()
        {
            // Act
            Action act = new(() => OracleQueryBuilder<EmptyEntity>.Select().Build());

            // Assert
            Assert.Throws<EmptyEntityException>(act);
        }

        #endregion

        #region SELECT BY ID

        [Fact]
        [Trait("SELECT", "BY ID")]
        public void SelectById_EntityWithPrimaryKey_GenerateOracleQuery()
        {
            // Arrange
            int idSearched = 1;

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().WhereId(idSearched).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectByIdSqlString(), query.QueryString);
        }

        [Fact]
        [Trait("SELECT", "BY ID")]
        public void SelectById_EntityWithPrimaryKey_WhereFilterContainsValues()
        {
            // Arrange
            int idSearched = 1;

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().WhereId(idSearched).Build();

            // Assert
            Assert.Equal(1, query.WhereFilter.Keys?.Count);
            Assert.Equal(idSearched, query.WhereFilter[nameof(ComplexEntity.FakeInt)]);
        }

        [Fact]
        [Trait("SELECT", "BY ID")]
        public void SelectById_EntityWithoutPrimaryKey_ThrowError()
        {
            // Arrange
            int idSearched = 1;

            // Act
            Action act = new(() => OracleQueryBuilder<SimpleEntity>.Select().WhereId(idSearched).Build());

            // Assert
            Assert.Throws<PrimaryKeyAttributeMissingException>(act);
        }

        [Fact]
        [Trait("SELECT", "BY ID")]
        public void SelectById_EntityWithCompositePrimaryKey_ThrowError()
        {
            // Arrange
            int idSearched = 1;

            // Act
            Action act = new(() => OracleQueryBuilder<CompositePrimaryEntity>.Select().WhereId(idSearched).Build());

            // Assert
            Assert.Throws<TooManyPrimaryKeyException>(act);
        }

        [Fact]
        [Trait("SELECT", "BY ID")]
        public void SelectById_EntityWithNullPrimaryKey_ThrowError()
        {
            // Arrange
            int? idSearched = null;

            // Act
            Action act = new(() => OracleQueryBuilder<ComplexEntity>.Select().WhereId(idSearched).Build());

            // Assert
            Assert.Throws<NullPrimaryKeyException>(act);
        }

        #endregion

        #region SELECT BY IDS

        [Fact]
        [Trait("SELECT", "BY IDS")]
        public void SelectByIds_EntityWithNullPrimaryKey_WhereFilterContainsValues()
        {
            // Arrange
            CompositePrimaryEntity compositePrimaryEntity = new()
            {
                FakeInt = 1,
                FakeString = "value"
            };

            // Act
            Query query = OracleQueryBuilder<CompositePrimaryEntity>.Select().WhereIds(compositePrimaryEntity).Build();

            // Assert
            Assert.Equal(2, query.WhereFilter.Keys?.Count);
            Assert.Equal(compositePrimaryEntity.FakeInt, query.WhereFilter[nameof(compositePrimaryEntity.FakeInt)]);
            Assert.Equal(compositePrimaryEntity.FakeString, query.WhereFilter[nameof(compositePrimaryEntity.FakeString)]);
        }

        [Fact]
        [Trait("SELECT", "BY IDS")]
        public void SelectByIds_EntityWithPrimaryKey_GenerateOracleQuery()
        {
            // Arrange
            ComplexEntity complexEntity = new()
            {
                FakeInt = 1
            };

            // Act
            Query query = OracleQueryBuilder<ComplexEntity>.Select().WhereIds(complexEntity).Build();

            // Assert
            Assert.Equal(ComplexEntity.GetSelectByIdSqlString(), query.QueryString);
        }

        [Fact]
        [Trait("SELECT", "BY IDS")]

        public void SelectByIds_EntityWithoutPrimaryKey_ThrowError()
        {
            // Arrange
            SimpleEntity simpleEntity = new()
            {
                FakeInt = 1
            };

            // Act
            Action act = new(() => OracleQueryBuilder<SimpleEntity>.Select().WhereIds(simpleEntity).Build());

            // Assert
            Assert.Throws<PrimaryKeyAttributeMissingException>(act);
        }

        [Fact]
        [Trait("SELECT", "BY IDS")]
        public void SelectByIds_EntityWithCompositePrimaryKey_GenerateOracleQuery()
        {
            // Arrange
            CompositePrimaryEntity compositePrimaryEntity = new()
            {
                FakeInt = 1,
                FakeString = "value"
            };

            // Act
            Query query = OracleQueryBuilder<CompositePrimaryEntity>.Select().WhereIds(compositePrimaryEntity).Build();

            // Assert
            Assert.Equal(CompositePrimaryEntity.GetSelectByIdsSqlString(), query.QueryString);
        }

        [Fact]
        [Trait("SELECT", "BY IDS")]
        public void SelectByIds_EntityWithNullPrimaryKey_ThrowError()
        {
            // Arrange
            CompositePrimaryEntity compositePrimaryEntity = new()
            {
                FakeInt = 0,
                FakeString = null
            };

            // Act
            Action act = new(() => OracleQueryBuilder<CompositePrimaryEntity>.Select().WhereIds(compositePrimaryEntity).Build());

            // Assert
            Assert.Throws<NullPrimaryKeyException>(act);
        }

        #endregion
    }
}