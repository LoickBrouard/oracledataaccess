﻿using OracleSqlBuilder;
using OracleSqlBuilder.Models;

using Test_Resources.FakeEntities;

using Xunit;

namespace Test_SqlBuilder
{
    public class ParsingNameTests
    {
        #region MAX NORMAL USE

        [Fact]
        [Trait("PARSING NAME", "PARSING FIELD ON SELECT")]
        public void ParsingName_EntityWithNameDifferentInDatabase_GenerateOracleSelectQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<NameSampleEntity>.Select().Build();
            // Assert
            Assert.Equal(NameSampleEntity.GetSelectSqlString(), query.QueryString);
        }

        [Fact]
        [Trait("PARSING NAME", "PARSING FIELD ON WHERE")]
        public void ParsingName_EntityWithNameDifferentInDatabase_GenerateOracleQueryWithWhere()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<AttributedEntity>.Select().Where(a => a.FakeDifferentNameEntity == "" & a.FakeInt == 1).Build();
            // Assert
            Assert.Equal(AttributedEntity.GetSelectWhereSqlString("(other_TableName.OTHERNAME=:FakeDifferentNameEntity)and(other_TableName.fake_int=:FakeInt)"), query.QueryString);
        }

        #endregion

    }
}
