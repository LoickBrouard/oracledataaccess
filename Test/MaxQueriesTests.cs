﻿using System;

using OracleSqlBuilder;
using OracleSqlBuilder.Exceptions;
using OracleSqlBuilder.Models;

using Test_Resources.FakeEntities;

using Xunit;

namespace Test_SqlBuilder
{
    public class MaxQueriesTests
    {
        #region MAX NORMAL USE

        [Fact]
        [Trait("MAX", "NORMAL USE")]
        public void SelectMax_ValueMatchingAnEntityPropertyName_GenerateOracleQuery()
        {
            // Arrange

            // Act
            Query query = OracleQueryBuilder<SimpleEntity>.Max(nameof(SimpleEntity.FakeInt)).Build();
            // Assert
            Assert.Equal("SELECT MAX(simple_entity.fake_int) FROM simple_entity", query.QueryString);
        }

        #endregion

        #region MAX ERROR

        [Fact]
        [Trait("MAX", "ERROR")]
        public void SelectMax_ValueNotMatchingAnEntityPropertyName_ThrowError()
        {
            // Arrange

            // Act
            Action act = new (()=> OracleQueryBuilder<SimpleEntity>.Max("notAProperty").Build());
            // Assert
            Assert.Throws<SqlBuilderException>(act);
        }

        #endregion
    }
}
