﻿using System.Diagnostics;

using Dapper;

using Oracle.ManagedDataAccess.Client;

namespace Test_Resources.Database
{
    public class DatabaseHandler : IDisposable
    {
        public const string CONNECTION_STRING = "Data Source=xxxxxx;Password=xxxxxx;User ID=xxxxxx";
        private readonly OracleConnection _dbConnection;
        private bool disposedValue;
        private readonly IEnumerable<Scheme> _schemes;

        public DatabaseHandler(IEnumerable<Scheme> schemeToExecutes)
        {
            _dbConnection = new OracleConnection(CONNECTION_STRING);
            _schemes = schemeToExecutes;

            ClearDb();

            InitDatabase();
            
            Trace.WriteLine("DbConnection for init db is closed");
        }

        public void InitDatabase()
        {
            if (_dbConnection.State == System.Data.ConnectionState.Closed)
                _dbConnection.Open();

            foreach(Scheme scheme in _schemes)
                _dbConnection.Execute(scheme.CreationScheme);

            if (_dbConnection.State == System.Data.ConnectionState.Open)
                _dbConnection.Close();
        }

        private void ClearDb()
        {
            if (_dbConnection.State == System.Data.ConnectionState.Closed)
                _dbConnection.Open();
            
            foreach (Scheme scheme in _schemes)
                _dbConnection.Execute(scheme.DeletionScheme);

            if (_dbConnection.State == System.Data.ConnectionState.Open)
                _dbConnection.Close();
            
            Trace.WriteLine("DbConnection for drop db is closed");
        }

        #region DISPOSING

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                ClearDb();
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
