﻿namespace Test_Resources.Database
{
    public class Scheme
    {
        public string CreationScheme { get; set; }
        public string DeletionScheme { get; private set; }

        public static Scheme TableScheme(string creationScheme, string tableName)
        {
            Scheme scheme = new (creationScheme);
            scheme.CreateTableDeletionScheme(tableName);

            return scheme;
        }

        internal static Scheme ProcedureScheme(string creationScheme, string procedureName)
        {
            Scheme scheme = new (creationScheme);
            scheme.CreateProcedureDeletionScheme(procedureName);

            return scheme;
        }
        private Scheme(string creationScheme)
        {
            CreationScheme = creationScheme;
        }

        private void CreateProcedureDeletionScheme(string procedureName)
        {
            DeletionScheme = $@"
                    DECLARE
                      V_NUM NUMBER;
                    BEGIN  
                    SELECT COUNT(*)
                    INTO   V_NUM
                    FROM   USER_OBJECTS
                    WHERE  OBJECT_NAME = '{procedureName}'
                    AND    OBJECT_TYPE = 'PROCEDURE'; 
                    IF V_NUM > 0 THEN
                    EXECUTE IMMEDIATE 'DROP PROCEDURE {procedureName}';
                    END IF;
                    END;";
        }

        private void CreateTableDeletionScheme(string tableName)
        {
            DeletionScheme = $@"BEGIN
                    EXECUTE IMMEDIATE 'DROP TABLE ' || '{tableName}';
                    EXCEPTION
                    WHEN OTHERS THEN
                    IF SQLCODE != -942 THEN
                    RAISE;
                    END IF;
                    END;";
        }

    }
}
