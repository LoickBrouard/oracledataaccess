﻿namespace Test_Resources.Helpers
{
    public static class ResourcesHelper
    {
        public static T[] GenerateList<T>(int capacity)
        {
            T[] list = new T[capacity];
            
            for(int i = 0; i < capacity; i++)
            {
                list[i] = default(T);
            }

            return list;
        }
    }
}
