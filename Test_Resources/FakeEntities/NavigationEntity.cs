﻿using Domain.Core.Primitives;

namespace Test_Resources.FakeEntities
{
    public class NavigationEntity : Entity
    {
        public int FakeId { get; set; }
    }
}
