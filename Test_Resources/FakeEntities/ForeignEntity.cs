﻿using Domain.Core.Primitives;

namespace Test_Resources.FakeEntities
{
    public class ForeignEntity : Entity
    {
        public int NavigationEntityId { get; set; }
        public int FakeForeignInt { get; set; }

        public virtual NavigationEntity NavigationEntity { get; set; }
    }
}
