﻿using Domain.Core.Attributes;
using Domain.Core.Primitives;

namespace Test_Resources.FakeEntities
{
    public class ComplexEntity : Entity
    {
        [PrimaryKey]
        public int FakeInt { get; set; }
        public string FakeString { get; set; }
        public DateTime FakeDatetime { get; set; }
        public bool FakeBool { get; set; }

        virtual public string VirtualProperty { get; set; }

        private const string SELECT_ALL_QUERY = "SELECT complex_entity.fake_int AS FakeInt,complex_entity.fake_string AS FakeString,complex_entity.fake_datetime AS FakeDatetime,complex_entity.fake_bool AS FakeBool";
        private const string INSERT_QUERY = "INSERT INTO complex_entity (fake_int,fake_string,fake_datetime,fake_bool) VALUES (:FakeInt,:FakeString,:FakeDatetime,:FakeBool)";
        private const string UPDATE_QUERY = "UPDATE complex_entity SET fake_int=:FakeInt,fake_string=:FakeString,fake_datetime=:FakeDatetime,fake_bool=:FakeBool";
        private const string DELETE_QUERY = "DELETE complex_entity";
        private const string FROM_TABLE_QUERY = "From complex_entity";

        public static string GetSelectSqlString()
            => $"{SELECT_ALL_QUERY} {FROM_TABLE_QUERY}";

        public static string GetSelectByIdSqlString()
            => $"{SELECT_ALL_QUERY} {FROM_TABLE_QUERY} WHERE complex_entity.fake_int=:FakeInt";

        public static IEnumerable<char> GetSelectWhereSqlString(string whereCondition)
            => $"{SELECT_ALL_QUERY} {FROM_TABLE_QUERY} WHERE ({whereCondition})";

        public static IEnumerable<char> GetInsertSqlQuery()
            => INSERT_QUERY;
        public static IEnumerable<char> GetUpdateSqlQuery(string whereCondition)
            => $"{UPDATE_QUERY} WHERE {whereCondition}";

        public static IEnumerable<char> GetDeleteSqlQuery(string whereCondition)
            => $"{DELETE_QUERY} WHERE {whereCondition}";
    }
}
