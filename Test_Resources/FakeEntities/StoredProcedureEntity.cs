﻿using Domain.Core.Interfaces;

using Test_Resources.Database;

namespace Test_Resources.FakeEntities
{
    public class StoredProcedure
    {
        public const string NAME = "FAKE_PROCEDURE";

        public static Scheme GetScheme()
        {
            return Scheme.ProcedureScheme(@"create or replace procedure FAKE_PROCEDURE(
                InFakeString in varchar2,
                InFakeInt in integer,
                InFakeDate in date,
                OutfakeInt OUT integer,
                OutfakeString OUT varchar2,
                Outsysdate OUT date   
                ) is
                begin
                OutfakeInt:= InFakeInt + 100;
                Outsysdate:=InFakeDate + INTERVAL '1' MINUTE;
                OutfakeString:= CONCAT('out',InFakeString);
                end;", "FAKE_PROCEDURE");
        }
    }
    public class StoredProcedureInput : IStoredProcedureInput
    {
        public StoredProcedureInput(string inFakeString, int inFakeInt, DateTime inFakeDate)
        {
            InFakeString = inFakeString;
            InFakeInt = inFakeInt;
            InFakeDate = inFakeDate;
        }

        public string InFakeString { get; set; }
        public int InFakeInt { get; set; }
        public DateTime InFakeDate { get; set; }
    }
    public class StoredProcedureOutput : IStoredProcedureOutput
    {
        public string OutfakeString { get; set; }
        public int OutfakeInt { get; set; }
        public DateTime Outsysdate { get; set; }
    }
}
