﻿using Domain.Core.Attributes;
using Domain.Core.Primitives;

namespace Test_Resources.FakeEntities
{
    public class CompositePrimaryEntity : Entity
    {
        [PrimaryKey]
        public int FakeInt { get; set; }
        [PrimaryKey]
        public string FakeString { get; set; }


        public static string GetSelectSqlString()
            => "SELECT composite_primary_entity.fake_int AS FakeInt,composite_primary_entity.fake_string AS FakeString From composite_primary_entity";

        public static string GetSelectByIdsSqlString()
            => "SELECT composite_primary_entity.fake_int AS FakeInt,composite_primary_entity.fake_string AS FakeString From composite_primary_entity WHERE composite_primary_entity.fake_int=:FakeInt AND composite_primary_entity.fake_string=:FakeString";
    }
}
