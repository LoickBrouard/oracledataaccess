﻿using Domain.Core.Primitives;

using Test_Resources.Database;

namespace Test_Resources.FakeEntities;

public class SimpleEntity : Entity, IEquatable<SimpleEntity>
{
    public int FakeInt { get; set; }
    public string FakeString { get; set; }

    private const string SELECT_ALL_QUERY = "SELECT simple_entity.fake_int AS FakeInt,simple_entity.fake_string AS FakeString";
    private const string INSERT_QUERY = "INSERT INTO simple_entity (fake_int,fake_string) VALUES (:FakeInt,:FakeString)";
    private const string FROM_TABLE_QUERY = "From simple_entity";

    public static string GetSelectSqlString()
        => $"{SELECT_ALL_QUERY} {FROM_TABLE_QUERY}";

    public static string GetSelectWhereSqlString(string whereExpected)
    => $"{SELECT_ALL_QUERY} {FROM_TABLE_QUERY} WHERE ({whereExpected})";

    public static string GetInsertSqlQuery()
        => INSERT_QUERY;

    public static Scheme GetScheme()
        => Scheme.TableScheme("CREATE TABLE SIMPLE_ENTITY(FAKE_INT NUMBER, FAKE_STRING VARCHAR2(50 BYTE))","SIMPLE_ENTITY");

    public override int GetHashCode() => $"{FakeInt}{FakeString}".GetHashCode();

    public override bool Equals(object? obj)
    {
        if (obj is null && this is null)
            return true;

        if(obj is SimpleEntity simpleEntity)
            return simpleEntity.FakeInt == FakeInt && simpleEntity.FakeString == FakeString;

        return false;
    }

    public bool Equals(SimpleEntity? other)
    {
        if (other is null && this is null)
            return true;

        if (other is SimpleEntity simpleEntity)
            return simpleEntity.FakeInt == FakeInt && simpleEntity.FakeString == FakeString;

        return false;
    }
}