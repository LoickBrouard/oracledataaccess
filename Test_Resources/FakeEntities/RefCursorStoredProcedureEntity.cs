﻿using Domain.Core.Attributes;
using Domain.Core.Interfaces;

using Test_Resources.Database;

namespace Test_Resources.FakeEntities
{
    public class RefCursorStoredProcedure
    {
        public const string NAME = "FAKE_REF_CURSOR_PROCEDURE";

        public static Scheme GetScheme()
        {
            return Scheme.ProcedureScheme(@"create or replace procedure FAKE_REF_CURSOR_PROCEDURE(
                InFakeString in varchar2,
                InFakeInt in integer,
                InFakeDate in date,
                OutfakeInt OUT integer,
                OutfakeString OUT varchar2,
                OutCursor OUT sys_refcursor,
                Outsysdate OUT date   
                ) is
                begin
                OutfakeInt:= InFakeInt + 100;
                Outsysdate:=InFakeDate + INTERVAL '1' MINUTE;
                OutfakeString:= CONCAT('out',InFakeString);
                OPEN OutCursor FOR SELECT AValue, OtherValue FROM ( SELECT 'a' AS AValue, 2 AS OtherValue FROM DUAL UNION ALL SELECT '3', 6 FROM DUAL UNION ALL SELECT 'c', 8 FROM DUAL );
                end;", NAME);
        }
    }
    public class RefCursorStoredProcedureInput : IStoredProcedureInput
    {
        public RefCursorStoredProcedureInput(string inFakeString, int inFakeInt, DateTime inFakeDate)
        {
            InFakeString = inFakeString;
            InFakeInt = inFakeInt;
            InFakeDate = inFakeDate;
        }

        public string InFakeString { get; set; }
        public int InFakeInt { get; set; }
        public DateTime InFakeDate { get; set; }
    }
    public class AnonymousRefCursorOutput : IStoredProcedureOutput
    {
        public string OutfakeString { get; set; }
        public int OutfakeInt { get; set; }
        public DateTime Outsysdate { get; set; }
        [RefCursor]
        public IEnumerable<object> OutCursor { get; set; }
    }

    public class RefCursorOutput : IStoredProcedureOutput
    {
        public string OutfakeString { get; set; }
        public int OutfakeInt { get; set; }
        public DateTime Outsysdate { get; set; }
        [RefCursor]
        public IEnumerable<refCursorObject> OutCursor { get; set; }
    }

    public class refCursorObject
    {
        public string AValue { get; set; }
        public decimal OtherValue { get; set; }
    }
}
