﻿using Domain.Core.Attributes;
using Domain.Core.Primitives;

namespace Test_Resources.FakeEntities
{
    public class NameSampleEntity : Entity
    {
        public string Singlewordentity { get; set; }
        public string MultipleWordEntity { get; set; }

        public string MultipleWordAndNumber1Entity { get; set; }
        
        [NameInDatabase("otherName_1_")]
        public string SeparatedWordAndNumber1Entity { get; set; }

        private const string TABLE_NAME= "name_sample_entity";
        private const string SELECT_ALL_QUERY = $"SELECT {TABLE_NAME}.singlewordentity AS Singlewordentity,{TABLE_NAME}.multiple_word_entity AS MultipleWordEntity,{TABLE_NAME}.multiple_word_and_number1_entity AS MultipleWordAndNumber1Entity,{TABLE_NAME}.otherName_1_ AS SeparatedWordAndNumber1Entity";
        private const string FROM_TABLE_QUERY = $"From {TABLE_NAME}";
        public static string GetSelectSqlString() => $"{SELECT_ALL_QUERY} {FROM_TABLE_QUERY}";
    }
}
