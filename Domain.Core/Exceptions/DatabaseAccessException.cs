﻿using System;

namespace Domain.Core.Exceptions
{
    public class DatabaseAccessException : Exception
    {
        public DatabaseAccessException() : base() { }
        public DatabaseAccessException(string message) : base(message) { }
        public DatabaseAccessException(string message, Exception innerException) : base(message, innerException) { }
    }
}
