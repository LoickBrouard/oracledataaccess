﻿using System;
using System.Collections.Generic;

using Domain.Core.Exceptions;
using Domain.Core.Primitives;

namespace Domain.Core.Interfaces
{
    /// <summary>
    /// Generate the unit of work object, connected to a Database
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Generate the repository corresponding to the entity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <exception cref="DatabaseException"></exception>
        /// <returns></returns>
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;

        /// <summary>
        /// Generate a StoredProcedure
        /// </summary>
        /// <param name="procedureName"></param>
        /// <returns></returns>
        IStoredProcedureOnGoing<TStoredProcedureInput, TStoredProcedureOutput> GetStoredProcedure<TStoredProcedureInput, TStoredProcedureOutput>(string procedureName) 
            where TStoredProcedureInput : IStoredProcedureInput
            where TStoredProcedureOutput : IStoredProcedureOutput;

        /// <summary>
        /// Generate a stored function
        /// </summary>
        /// <param name="functionName"></param>
        /// <returns></returns>
        IStoredFunction ExecuteStoredFunction(string functionName);

        /// <summary>
        /// Rollback all queries within the current transaction
        /// </summary>
        void Rollback();

        /// <summary>
        /// Commit all queries within the current transaction
        /// </summary>
        void SaveChanges();

        /// <summary>
        /// Return the database date and time
        /// </summary>
        /// <returns></returns>
        DateTime GetDatabaseDateTime();

        /// <summary>
        /// <b>FOR SELECT QUERY ONLY</b><br/>
        /// <br/>
        /// <b>To modify the database you can use <see cref="ExecuteFromRawSql" /></b><br/>
        /// <br/>
        /// Return a query result mapped onto the <see href="TReturnObject"/> passed
        /// </summary>
        /// <remarks>
        /// Exemple : 
        /// <example>
        /// This shows how to increment an integer.
        /// <code>
        /// IEnumerable&lt;object&gt; specificSqlQuery = _unitOfWork.QueryFromRawSql(sql: @"
        /// select
        /// b.NUM_CARTE as numCarte,
        /// p2.CLIENT as client,
        /// b.etat as AutreChose,
        /// b.num_campagne as campagne
        /// from boitier b
        /// INNER JOIN produit2 P2 on b.type_produit = p2.type_produit
        /// where p2.client = :client
        /// and num_interdiction = :interdiction",
        /// parameters: new { client = "RENAULT", interdiction = "M000061501" },
        /// returnModel: new { numCarte = "", client = "", AutreChose = "", campagne = 0 });
        /// </code>
        /// </example>
        /// </remarks>
        /// <typeparam name="TReturnObject"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="returnModel"></param>
        /// <returns></returns>
        IEnumerable<TReturnObject> QueryFromRawSql<TReturnObject>(string sql, object parameters, TReturnObject returnModel);

        /// <summary>
        /// <b>FOR DELETE, UPDATE, INSERT QUERY</b><br/>
        /// <br/>
        /// <b>To read database, you can use <see cref="QueryFromRawSql{TReturnObject}" /></b><br/>
        /// <br/>
        /// Return a query result mapped onto the <see href="TReturnObject"/> passed
        /// </summary>
        /// <remarks>
        /// Exemple : 
        /// <example>
        /// This shows how to increment an integer.
        /// <code>
        /// IEnumerable&lt;object&gt; specificSqlQuery = _unitOfWork.QueryFromRawSql(sql: @"
        /// select
        /// b.NUM_CARTE as numCarte,
        /// p2.CLIENT as client,
        /// b.etat as AutreChose,
        /// b.num_campagne as campagne
        /// from boitier b
        /// INNER JOIN produit2 P2 on b.type_produit = p2.type_produit
        /// where p2.client = :client
        /// and num_interdiction = :interdiction",
        /// parameters: new { client = "RENAULT", interdiction = "M000061501" },
        /// returnModel: new { numCarte = "", client = "", AutreChose = "", campagne = 0 });
        /// </code>
        /// </example>
        /// </remarks>
        /// <typeparam name="TReturnObject"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="returnModel"></param>
        /// <returns></returns>
        void ExecuteFromRawSql(string sqlQuery, object parameters);

    }
}