﻿using Domain.Core.Exceptions;
using Domain.Core.Primitives;

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain.Core.Interfaces
{
    /// <summary>
    /// Generic methods allowing to manipulate an entity in database
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> where TEntity : Entity
    {
        /// <summary>
        /// To get information filtered by the PrimaryKey attribute
        /// </summary>
        /// <param name="id">Clef primaire de l'entité</param>
        /// <returns></returns>
        /// <exception cref="DatabaseException"></exception>
#if NETCOREAPP || NETSTANDARD2_1
        TEntity? GetById(object id);
#else
        // Fallback behavior for old versions.
        TEntity GetById(object id);
#endif
        /// <summary>
        /// To get information filtered by the PrimaryKey attribute
        /// </summary>
        /// <param name="entity">Clef primaire de l'entité</param>
        /// <returns></returns>
        /// <exception cref="DatabaseException"></exception>
#if NETCOREAPP || NETSTANDARD2_1
        TEntity? GetById(TEntity entity);
#else
        // Fallback behavior for old versions.
        TEntity GetById(TEntity entity);
#endif
        /// <summary>
        /// Return the first result matching the predicate expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        /// <exception cref="DatabaseException"></exception>
#if NETCOREAPP || NETSTANDARD2_1
        TEntity? GetFirst(Expression<Func<TEntity, bool>> expression);
#else
        // Fallback behavior for old versions.
        TEntity GetFirst(Expression<Func<TEntity, bool>> expression);
#endif

        /// <summary>
        /// To get information filtered by a lambda expression
        /// </summary>
        /// <param name="expression">Expression lambda permettant d'alimenter la clause where</param>
        /// <returns></returns>
        /// <exception cref="DatabaseException"></exception>
        IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> expression);

        /// <summary>
        /// To get the number of rows in a table
        /// Récupération du nombre d'entrées d'une table entière
        /// </summary>
        /// <returns></returns>
        /// <exception cref="DatabaseException"></exception>
        int Count();

        /// <summary>
        /// To get the number of rows in a table matching all conditions of a lambda expression
        /// </summary>
        /// <param name="expression">Expression lambda permettant d'alimenter la clause where</param>
        /// <returns></returns>
        /// <exception cref="DatabaseException"></exception>
        int Count(Expression<Func<TEntity, bool>> expression);

        /// <summary>
        /// Return the maximum value of a property
        /// </summary>
        /// <param name="propertySearched"></param>
        /// <exception cref="DatabaseException"></exception>
        TReturnType Max<TReturnType>(string propertySearched);

        /// <summary>
        /// Return the maximum value of a property filtered by a lambda expression
        /// </summary>
        /// <param name="propertySearched"></param>
        /// <param name="expression">Expression lambda permettant d'alimenter la clause where</param>
        /// <exception cref="DatabaseException"></exception>
        TReturnType Max<TReturnType>(string propertySearched, Expression<Func<TEntity, bool>> expression);

        /// <summary>
        /// Return if the data exists matching the PrimaryKey attribute
        /// </summary>
        /// <param name="primaryKey">Clef primaire de l'entité</param>
        /// <returns></returns>
        /// <exception cref="DatabaseException"></exception>
        bool Exists(object primaryKey);

        /// <summary>
        /// To get all the information of a specific table
        /// </summary>
        /// <returns></returns>
        /// <exception cref="DatabaseException"></exception>
        IEnumerable<TEntity> GetAll();
        /// <summary>
        /// To get the result of <typeparamref name="TEntity"/> join with <typeparamref name="TJoinedEntity"/>
        /// </summary>
        /// <typeparam name="TJoinedEntity"></typeparam>
        /// <param name="mapFunction"></param>
        /// <param name="junction"></param>
        /// <param name="joinType"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        /// <exception cref="DatabaseException"></exception>
        IEnumerable<TEntity> GetJoin<TJoinedEntity>(Func<TEntity, TJoinedEntity, TEntity> mapFunction, Expression<Func<TEntity, TJoinedEntity, bool>> junction, Join joinType = Join.Left, Expression<Func<TEntity, bool>> expression = null) where TJoinedEntity : Entity;

        /// <summary>
        /// Insert the entity in database
        /// </summary>
        /// <param name="entity">L'entité à insérer</param>
        /// <exception cref="DatabaseException"></exception>
        TEntity Insert(TEntity entity);

        /// <summary>
        /// Update an entity based on the PrimaryKey attribute
        /// </summary>
        /// <param name="entity">L'entité à mettre à jour</param>
        /// <exception cref="DatabaseException"></exception>
        TEntity Update(TEntity entity);

        /// <summary>
        /// Delete an entity based on the PrimaryKey attribute
        /// </summary>
        /// <param name="primaryKey">La clef primaire</param>
        /// <exception cref="DatabaseException"></exception>
        void Delete(TEntity entity);
    }
}
