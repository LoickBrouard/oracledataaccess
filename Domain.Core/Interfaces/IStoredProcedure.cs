﻿namespace Domain.Core.Interfaces
{
    /// <summary>
    /// Generic methods allowing to handle a StoredProcedure within a database
    /// </summary>
    public interface IStoredProcedure { }

    /// <summary>
    /// Define a store procedure in preparation, allow to set input
    /// </summary>
    /// <typeparam name="TStoredProcedureInput"></typeparam>
    /// <typeparam name="TStoredProcedureOutput"></typeparam>
    public interface IStoredProcedureOnGoing<TStoredProcedureInput, TStoredProcedureOutput>
        where TStoredProcedureInput : IStoredProcedureInput
        where TStoredProcedureOutput : IStoredProcedureOutput
    {

        /// <summary>
        /// Set the input on the store procedure transaction
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        IStoredProcedureOnGoing<TStoredProcedureInput, TStoredProcedureOutput> WithInput(IStoredProcedureInput input);
        
        /// <summary>
        /// Execute the stored procedure
        /// </summary>
        /// <returns></returns>
        TStoredProcedureOutput Execute();

        /// <summary>
        /// Execute the stored procedure with specific mapping for refCursor property
        /// </summary>
        /// <returns></returns>
        TStoredProcedureOutput ExecuteWithRefCursor<TRefCursor>();
    }

    /// <summary>
    /// List the properties of a store procedure of type IN
    /// </summary>
    public interface IStoredProcedureInput { }

    /// <summary>
    /// List the properties of a store procedure of type OUT
    /// </summary>
    public interface IStoredProcedureOutput { }
}
