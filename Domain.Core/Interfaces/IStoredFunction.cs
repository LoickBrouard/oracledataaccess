﻿namespace Domain.Core.Interfaces
{
    public interface IStoredFunction
    {
        /// <summary>
        /// Execute with inputs and retun the result of the stored function
        /// </summary>
        /// <typeparam name="TStoredFunctionOutput"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        TStoredFunctionOutput WithInput<TStoredFunctionOutput>(IStoredFunctionInput input);
    }
}
