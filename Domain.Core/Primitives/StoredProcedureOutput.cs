﻿using Domain.Core.Interfaces;

namespace Domain.Core.Primitives
{
    public abstract class StoredProcedureOutput : IStoredProcedureOutput
    {
    }
}
