﻿namespace Domain.Core.Primitives
{
    public enum Join
    {
        Left,
        Right,
        Full,
        Inner
    }
}
