﻿using System.Collections.Generic;

namespace Domain.Core.Primitives
{
    public static class EntityExtensions
    {
        /// <summary>
        /// To find all value matching the like comparison with the like expression value
        /// <example>
        /// <code>
        /// %value% --> To match everything containing the word "value"
        /// value%  --> To match everything begining by "value"
        /// %value  --> To match everything ending with "value"
        /// </code>
        /// </example>
        /// </summary>  
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="property"></param>
        /// <param name="likeExpression"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        public static bool Like<TEntity>(this TEntity entity, object property, string likeExpression, bool caseSensitive) where TEntity : Entity
            => true;

        /// <summary>
        /// To find all value NOT matching the like comparison with the like expression value
        /// <example>
        /// <code>
        /// %value% --> To match everything NOT containing the word "value"
        /// value%  --> To match everything NOT begining by "value"
        /// %value  --> To match everything NOT ending with "value"
        /// </code>
        /// </example>
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="property"></param>
        /// <param name="likeExpression"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        public static bool NotLike<TEntity>(this TEntity entity, object property, string likeExpression, bool caseSensitive) where TEntity : Entity
            => true;

        /// <summary>
        /// To find all values contained in the array param
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="property"></param>
        /// <param name="array"></param>
        /// <returns></returns>
        public static bool In<TEntity, TArrayType>(this TEntity entity, TArrayType property, IEnumerable<TArrayType> array) where TEntity : Entity
            => true;
    }
}
