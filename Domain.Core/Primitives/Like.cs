﻿namespace Domain.Core.Primitives
{
    public class Like
    {
        public string LikeQueryString { get; }
        public string PropertyName { get; }
        public bool CaseSensitive { get; }

        public Like(string propertyName, string likeExpression, bool caseSensitive)
        {
            PropertyName = propertyName;
            LikeQueryString = likeExpression;
            CaseSensitive = caseSensitive;
        }
    }
}
