﻿using System;

namespace Domain.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SysdateOnInsertAttribute : Attribute
    {
    }
}
