﻿using System;

namespace Domain.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class StoredItemParameterAttribute : Attribute
    {
        public int Size { get; set; }
        public StoredItemParameterAttribute(int size)
        {
            Size = size;
        }
    }
}
