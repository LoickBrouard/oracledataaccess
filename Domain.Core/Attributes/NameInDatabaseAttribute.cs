﻿using System;

namespace Domain.Core.Attributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class NameInDatabaseAttribute : Attribute
    {
        public string NameInDataBase { get; set; }
        public NameInDatabaseAttribute(string nameInDataBase)
        {
            if (string.IsNullOrEmpty(nameInDataBase))
                throw new ArgumentNullException("nameInDataBase", "The nameInDatabaseAtribute has an empty string as argument");

            NameInDataBase = nameInDataBase;
        }
    }
}
