<br/>
<p align="center">
  <h3 align="center">Generic repository, Dapper, Oracle</h3>

  <p align="center">
    For legacy database that cannot be easily transform with a code first approach
    <br/>
    <br/>
  </p>
</p>



## Table Of Contents

* [About the Project](#about-the-project)
* [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [License](#license)
* [Authors](#authors)
* [Acknowledgements](#acknowledgements)

## About The Project

Created at first to have a single access point to a huge legacy Oracle Database. It was meant to be a library managing the database access and gathering all Oracle SQL queries. 

It started to act as an ORM with Dapper and reflection mechanisms used to generate SQL queries from an object.

And finally its implementation was proposed using the generic repsitory pattern to cover 99% of the legacy code.

## Built With

This project was built with the less external dependencies as possible

* [Dapper](https://github.com/DapperLib/Dapper)
* [.NET Core](https://github.com/dotnet/aspnetcore)
* [XUnit](https://github.com/xunit/xunit)
* [Dapper.Oracle](https://github.com/DIPSAS/Dapper.Oracle)
* [ReadME-Generator 🙏](https://github.com/ShaanCoding/ReadME-Generator)

## Getting Started

This is the basic setup to use this solution

### Prerequisites

To start the project you would need an Oracle Database.

### Installation

1. Clone this project, run it and build it.
2. Find an OracleDataAccess.nupkg package in the "Generated Nuget" folder.
3. Reference the nuGet in your solution
4. Reference the IUnitOfWork and the connection string

* With dependency injection, add the UnitOfWork class into your DI process : 
```csharp
services.AddScoped<IUnitOfWork>( _ => new UnitOfWork(connectionString));
```
* Without dependency injection you can use it like this : 
```csharp
// Do not forget the "using" part to be sure to close the connection / transaction after using it.
using (IUnitOfWork _unitOfWork = new UnitOfWork(connectionString))
{
    // Usage
}
```
5. Create a class reflecting one of your database table

```csharp
using System;

using Domain.Core.Primitives;

namespace Business.Entities.Entities
{
    public class EntityExample : Entity
    {
        public EntityExample() { }
        public int FakeInt { get; set; }
        public string FakeString { get; set; }
        public DateTime? FakeDatetime { get; set; }
        public decimal FakeDecimal { get; set; }
        public bool FakeBool { get; set; }
    }
}
```

## Usage

You are ready to use the nuGet :)
Now you just need to call the repository and select the method you want to interact with the database.

**"SELECT \*" query :**
```csharp
IEnumerable<EntityExample> entityExamples = _unitOfWork
                    .GetRepository<EntityExample>()
                    .GetAll();
```

**"INSERT INTO" transaction query :**
```csharp
// The bool will be converted into 1 or 0
EntityExample insertedEntity = _unitOfWork
    .GetRepository<EntityExample>()
    .Insert(new EntityExample()
    {
        FakeBool = true,
        FakeInt = 1,
    });
if(everythingOk)
    _unitOfWork.SaveChanges(); //Transaction comitted
else
    _unitOfWork.Rollback(); // Transaction rollback
```

**To run a stored procedure :**
* Create first the classes of inputs and output as per :
```csharp
    public class StoredProcedureExample : IStoredProcedureInput
    {
        public const string PROCEDURE_NAME = "Stored_Procedure_Name";
        public string InputString { get; set; }
    }
    public class StoredProcedureExampleOutput : IStoredProcedureOutput
    {
        public string OutputString { get; set; }
        public int OutputInt { get; set; }
        [RefCursor]
        public IEnumerable<OtherObject> OutputObjectArray{ get; set; }
    }

    public class OtherObject
    {
        public int OneProperty { get; set; }
        public string OtherProperty { get; set; }
    }
```
* And use it :
```csharp
StoredProcedureExampleOutput result = _unitOfWork
    .GetStoredProcedure<StoredProcedureExample, StoredProcedureExampleOutput>(StoredProcedureExample.PROCEDURE_NAME)
    .WithInput(new StoredProcedureExample()
    {
        InputString = "my input"
    })
    .Execute();
```

## Roadmap

* To rename functions and add some interfaces to make it compatible with every other database (With one configuration file per database)

* To reenforce unit testing and add missing ones (stored procedure)

*To ease the use of Joined query

### Creating A Pull Request



## License

Distributed under the MIT License.

## Authors

* **Loïck Brouard** - *junior Dev.* - [Loïck Brouard](https://gitlab.com/LoickBrouard) - *Blood, Sweat and Headache to create this "Dapper helper"*

## Acknowledgements

* [Nathan Fleuret](https://gitlab.com/nathanfleuret)
