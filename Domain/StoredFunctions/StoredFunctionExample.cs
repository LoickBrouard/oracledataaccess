﻿using Domain.Core.Interfaces;

namespace Business.Entities.StoredFunctions
{
    /// <summary>
    /// For now name must be the same than in the database
    /// </summary>
    public class StoredFunctionExample : IStoredFunctionInput
    {
        public string inString { get; set; }

        public StoredFunctionExample() { }
    }
}
