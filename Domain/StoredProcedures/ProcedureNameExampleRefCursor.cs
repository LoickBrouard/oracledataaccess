﻿using System.Collections.Generic;

using Domain.Core.Attributes;
using Domain.Core.Interfaces;

namespace Business.Entities.StoredProcedures
{
    public class RefCursorValue
    {
        public int Annt { get; set; }
        public int AnOtherInt { get; set; }
    }

    public class ProcedureNameExampleRefCursor : IStoredProcedureInput
    {
        public const string PROCEDURE_NAME = "STORED_PROCEDURE_NAME";
        public string InString { get; set; }
        public string InOtherString { get; set; }
    }
    public class CheckPanelAuthorizationOutput : IStoredProcedureOutput
    {
        public decimal OutnumberOfRecords { get; set; }
        public string OuterrorString { get; set; }
        [RefCursor]
        public IEnumerable<RefCursorValue> OutRefCursor { get; set; }
    }
}
