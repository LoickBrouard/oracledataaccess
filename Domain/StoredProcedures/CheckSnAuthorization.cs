﻿using System.Collections;
using System.Collections.Generic;

using Domain.Core.Attributes;
using Domain.Core.Interfaces;

namespace Business.Entities.StoredProcedures
{
    public class StoredProcedureExample : IStoredProcedureInput
    {
        public const string PROCEDURE_NAME = "Stored_Procedure_Name";
        public string InputString { get; set; }
    }
    public class StoredProcedureExampleOutput : IStoredProcedureOutput
    {
        public string OutputString { get; set; }
        public int OutputInt { get; set; }
        [RefCursor]
        public IEnumerable<OtherObject> OutputObjectArray{ get; set; }
    }

    public class OtherObject
    {
        public int OneProperty { get; set; }
        public string OtherProperty { get; set; }
    }
}
