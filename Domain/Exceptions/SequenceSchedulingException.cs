﻿using System;

namespace Domain.Traca.Exceptions
{

	[Serializable]
	public class SequenceSchedulingException : Exception
	{
		public SequenceSchedulingException(string message) : base(message) { }
		protected SequenceSchedulingException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}
