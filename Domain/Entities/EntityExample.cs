﻿using System;

using Domain.Core.Primitives;

namespace Business.Entities.Entities
{
    public class EntityExample : Entity
    {
        public EntityExample() { }
        public int FakeInt { get; set; }
        public string FakeString { get; set; }
        public DateTime? FakeDatetime { get; set; }
        public decimal FakeDecimal { get; set; }
        public bool FakeBool { get; set; }
    }
}
