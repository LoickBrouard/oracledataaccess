﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

using Domain.Core.Primitives;

using OracleSqlBuilder.Exceptions.LambdaVisitorExceptions;
using OracleSqlBuilder.Models;

namespace OracleSqlBuilder.Helpers
{
    // This class has been adapted from : https://www.codeproject.com/Articles/1241363/Expression-Tree-Traversal-Via-Visitor-Pattern-in-P

    internal class WhereLambdaVisitor : ExpressionVisitor
    {
        private string _currentParameterName = string.Empty;
        private readonly string _tableName;
        private readonly Type _entityType;
        private readonly StringBuilder _queryStringBuilder;
        private readonly Stack<string> _fieldNames;
        private Dictionary<string, object> _parameters;
        private int currentIndex = 1;
        const int ColumnBase = 26;
        const int DigitMax = 7; // ceil(log26(Int32.Max))
        const string Digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public string GetNextLetters()
        {
            if (currentIndex <= 0)
                throw new IndexOutOfRangeException("index must be a positive number");

            if (currentIndex <= ColumnBase)
            {
                string letter = Digits[currentIndex - 1].ToString();
                currentIndex++;
                return letter;
            }

            var sb = new StringBuilder().Append(' ', DigitMax);
            var current = currentIndex;
            var offset = DigitMax;
            while (current > 0)
            {
                sb[--offset] = Digits[--current % ColumnBase];
                current /= ColumnBase;
            }

            currentIndex++;

            return sb.ToString(offset, DigitMax - offset);
        }

        private WhereLambdaVisitor(string tableName, Type entityType)
        {
            _queryStringBuilder = new StringBuilder();
            _fieldNames = new Stack<string>();
            _parameters = new Dictionary<string, object> { };
            _tableName = tableName;
            _entityType = entityType;
        }

        //entry point
        public static Query GetOracleWhereQueryFromLambda(LambdaExpression predicate, string tableName)
        {
            WhereLambdaVisitor whereLambdaVisitor = new WhereLambdaVisitor(tableName, predicate.Parameters[0].Type);
            whereLambdaVisitor.Visit(predicate.Body);

            string query = whereLambdaVisitor._queryStringBuilder.ToString();
            Query result = new Query(query, whereLambdaVisitor._parameters);

            return result;
        }

        /// <summary>
        /// To parse the comparison operator encounter in the lambda
        /// <br/>(Corresponds to: and, or, greater than, less than, etc.)
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        /// <exception cref="LogicalOperatorUnknownException"></exception>
        protected override Expression VisitBinary(BinaryExpression node)
        {

            _queryStringBuilder.Append("(");

            Visit(node.Left);

            if (node.Right is ConstantExpression expression && expression.Value == null)
                _queryStringBuilder.Append($" {OracleConfiguration.NULL_LOGICAL_OPERATOR} ");

            else
            {
                if (!OracleConfiguration.LOGICAL_OPERATORS.ContainsKey(node.NodeType))
                    throw new LogicalOperatorUnknownException(node.NodeType.ToString());

                _queryStringBuilder.Append($"{OracleConfiguration.LOGICAL_OPERATORS[node.NodeType]}");
            }

            Visit(node.Right);

            _queryStringBuilder.Append(")");

            return node;
        }

        /// <summary>
        /// To capture the Like() method within lambda expression
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        /// <exception cref="MethodCallUnknownException"></exception>
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            // Cas d'une recherche de type LIKE
            if (node.NodeType == ExpressionType.Call
                && node.Method.Name == nameof(EntityExtensions.Like)
                && node.Method.DeclaringType == typeof(EntityExtensions))
                TreatLike(node, isNotLike: false);

            // Cas d'une recherche de type NOT LIKE
            else if (node.NodeType == ExpressionType.Call
                && node.Method.Name == nameof(EntityExtensions.NotLike)
                && node.Method.DeclaringType == typeof(EntityExtensions))
                TreatLike(node, isNotLike: true);

            // Cas d'une recherche de type IN
            else if (node.NodeType == ExpressionType.Call
                && node.Method.Name == nameof(EntityExtensions.In)
                && node.Method.DeclaringType == typeof(EntityExtensions))
                TreatIn(node);

            else
                throw new MethodCallUnknownException(node.Method.Name);

            return node;
        }

        protected override Expression VisitNew(NewExpression node)
        {
            if (node.Type == typeof(DateTime))
            {
                DateTime dateTime = Expression.Lambda<Func<DateTime>>(node).Compile()();
                _queryStringBuilder.Append(AddParameterAndReturnQuery(_currentParameterName, dateTime));
            }
            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {

            if (node.Type == typeof(DateTime))
            {
                if (node.Expression == null) // Simple MemberAccess like DateTime.Now
                {
                    var lambda = Expression.Lambda<Func<DateTime>>(node);
                    var dateTime = lambda.Compile()();
                    _queryStringBuilder
                        .Append(AddParameterAndReturnQuery(_currentParameterName, dateTime));
                    return node;
                }
                else if (node.Expression.NodeType == ExpressionType.Parameter)
                {
                    _currentParameterName = node.Member.Name;
                    _queryStringBuilder.Append($"{_tableName}.{GetNameInDatabase.From(node.Member.Name, _entityType)}");
                    return node;
                }
            }

            // Correspond to a constant
            if (node.Expression.NodeType == ExpressionType.Constant
               || node.Expression.NodeType == ExpressionType.MemberAccess)
            {
                _fieldNames.Push(node.Member.Name);
                Visit(node.Expression);
            }
            else
            {
                //corresponds to: x.Customer - just write "Customer"
                _currentParameterName = node.Member.Name;
                _queryStringBuilder.Append($"{_tableName}.{GetNameInDatabase.From(node.Member.Name, _entityType)}");
            }
            return node;
        }

        /// <summary>
        /// Treatment of constant part of lambda : 1, "Tom", instance of NameSpace+<>c__DisplayClass12_0, instance of Order, i.e.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected override Expression VisitConstant(ConstantExpression node)
        {
            //just write value
            if (node.Value is null)
            {
                _queryStringBuilder.Append(OracleConfiguration.NULL_VALUE);
                return node;
            }

            _queryStringBuilder.Append(AddParameterAndReturnQuery(_currentParameterName, GetValueFromConstantNode(node.Value)));

            return node;
        }

        private object GetValueFromConstantNode(object nodeValue, Type typeForced = null)
        {
            Type type = typeForced ?? nodeValue.GetType();

            //if it is not simple value
            if (type == typeof(string) || _fieldNames.Count <= 0)
                return nodeValue;
            else
            {
                //proper order of selected names provided by means of Stack structure
                var fieldName = _fieldNames.Pop();

                var fieldInfo = type.GetField(fieldName);
                object value;

                //get instance of order    
                if (fieldInfo != null)
                    value = fieldInfo.GetValue(nodeValue);

                //get value of "Customer" property on order
                else
                    value = type.GetProperty(fieldName).GetValue(nodeValue);

                return GetValueFromConstantNode(value);
            }
        }

        private TvalueType GetValueFromOutsideVariable<TvalueType>(MemberExpression memberExpression) where TvalueType : class
                => (TvalueType)GetValueFrom(memberExpression);

        private IEnumerable<TvalueType> GetArrayValueFromOutsideVariable<TvalueType>(MemberExpression memberExpression) where TvalueType : class
        {
            object value = GetValueFrom(memberExpression);

            if (value == null)
                throw new LambdaVisitorException("The In parameter must not be null");

            if (value.GetType().GetInterface(nameof(IEnumerable)) == null)
                throw new LambdaVisitorException("The In parameter must be an iterable item (list or array)");

            if(!((IEnumerable)value).GetEnumerator().MoveNext())
                throw new LambdaVisitorException("The list must not be empty");

            foreach (var item in (IEnumerable)value)
                yield return (TvalueType)item;
        }

        private object GetValueFrom(MemberExpression memberExpression)
        {
            string fieldName = memberExpression.Member.Name;
            ConstantExpression constantExpression = memberExpression.Expression as ConstantExpression;

            var fieldInfo = constantExpression.Type.GetField(fieldName);

            if (fieldInfo == null)
                throw new LambdaVisitorException($"Impossible to find the field information from {constantExpression.Type}");

            return fieldInfo.GetValue(constantExpression.Value);
        }

        private IEnumerable<TReturnType> GetListFromNewArrayExpression<TReturnType>(NewArrayExpression newArrayExpression) where TReturnType : class
        {
            List<TReturnType> result = new List<TReturnType>();

            foreach (ConstantExpression expression in newArrayExpression.Expressions.Cast<ConstantExpression>())
                result.Add(expression.Value as TReturnType);
            return result;
        }

        private IEnumerable<T> ExtractInValue<T>(Expression expression) where T : class
        {
            if (expression is MemberExpression memberExpression)
                return GetArrayValueFromOutsideVariable<T>(memberExpression);

            if (expression is NewArrayExpression newArrayExpression)
                return GetListFromNewArrayExpression<T>(newArrayExpression);

            if (expression is NewExpression newExpression && newExpression.Members is null)
                throw new LambdaVisitorException("The array used for the In method cannot be empty");

            if (expression is ConstantExpression constantExpression && constantExpression.Value is null)
                throw new LambdaVisitorException("The array used for the In method cannot be null");

            throw new LambdaVisitorException("The value used for the In method cannot be interpreted");
        }

        private string ExtractLikeValue(Expression expression)
        {
            if (expression is MemberExpression memberExpression)
                return GetValueFromOutsideVariable<string>(memberExpression);

            else if (expression is MethodCallExpression methodCallExpression)
            {
                if (Expression.Lambda(methodCallExpression).Compile().DynamicInvoke() is string result)
                    return result;
            }

            else if (expression is BinaryExpression binaryExpression)
                return $"{ExtractLikeValue(binaryExpression.Left)}{ExtractLikeValue(binaryExpression.Right)}";

            else if (expression is ConstantExpression constantExpression)
                return constantExpression.ToString().Replace("\"", "");

            throw new LambdaVisitorException("The value used for the like method cannot be interpreted");
        }

        private string AddParameterAndReturnQuery(string parameterName, object value)
        {
            if (_parameters.ContainsKey(parameterName))
                parameterName += GetNextLetters();

            _parameters[parameterName] = value;

            return OracleConfiguration.ReturnParameterNameWhithSymbol(parameterName);
        }

        private string AddParameterAndReturnQuery(string parameterName, IEnumerable<object> values)
        {
            Queue<string> parameterCache = new Queue<string>();
            foreach (object value in values)
            {
                if (_parameters.ContainsKey(parameterName))
                    parameterName += GetNextLetters();

                _parameters[parameterName] = value;
                parameterCache.Enqueue(OracleConfiguration.ReturnParameterNameWhithSymbol(parameterName));
            }
            return string.Join(",", parameterCache);
        }

        private void TreatLike(MethodCallExpression node, bool isNotLike)
        {
            ReadOnlyCollection<Expression> nodeArgs = node.Arguments;

            if (nodeArgs.Count < 4 || nodeArgs[1] == null || nodeArgs[2] == null || nodeArgs[3] == null)
                throw new LambdaVisitorException("One parameter of the 'like' method is missing or null");

            if (!(nodeArgs[1] is MemberExpression))
                throw new LambdaVisitorException("The like value must be string type");
            Type typ = ((ParameterExpression)nodeArgs[0]).GetType();
            string propertyName = ((MemberExpression)nodeArgs[1]).Member.Name.ToString().Replace("\"", "");

            if (propertyName.Contains("."))
                propertyName = propertyName.Split('.')[1];

            string whereValue = ExtractLikeValue(nodeArgs[2]);

            if (!whereValue.Contains("%"))
                throw new LambdaVisitorException("You cannot perform a research with no % symbol");

            if (whereValue == "%%")
                throw new LambdaVisitorException("You cannot perform a like research with no value (like %%)");

            bool caseSensitve = ((ConstantExpression)nodeArgs[3]).Value.Equals(true);
            _currentParameterName = "likeParameter";

            Like likeExpression = new Like(propertyName.ToString(), whereValue, caseSensitve);

            string parameterValue = likeExpression.LikeQueryString.Replace("%", "");

            _queryStringBuilder.Append("(");

            switch (likeExpression.CaseSensitive)
            {
                case true:
                    _queryStringBuilder.Append($"{_tableName}.{GetNameInDatabase.From(likeExpression.PropertyName, _entityType)}");
                    break;
                case false:
                    parameterValue = parameterValue.ToLower();
                    _queryStringBuilder.Append($"LOWER({_tableName}.{GetNameInDatabase.From(likeExpression.PropertyName, _entityType)})");
                    break;
            }

            _queryStringBuilder
                .Append(isNotLike ? " NOT LIKE " : " LIKE ")
                .Append(caseSensitve ? "" : "LOWER(")
                .Append(likeExpression.LikeQueryString.StartsWith("%") ? "'%' || " : "")
                .Append(AddParameterAndReturnQuery(_currentParameterName, parameterValue))
                .Append(likeExpression.LikeQueryString.EndsWith("%") ? " || '%'" : "")
                .Append(caseSensitve ? "" : ")")
                .Append(")");
        }

        private void TreatIn(MethodCallExpression node)
        {
            ReadOnlyCollection<Expression> nodeArgs = node.Arguments;

            if (nodeArgs.Count < 3 || nodeArgs[1] == null || nodeArgs[2] == null)
                throw new LambdaVisitorException("One parameter of the 'in' method is missing or null");

            string propertyName = ((MemberExpression)nodeArgs[1])
                .Member
                .Name
                .ToString()
                .Replace("\"", "");

            propertyName = (propertyName.Contains(".")) ?
                propertyName.Split('.')[1]
                : propertyName;
            
            IEnumerable<object> whereValue = ExtractInValue<object>(nodeArgs[2]);

            if (whereValue == null || !whereValue.Any())
                throw new LambdaVisitorException("Something went wrong when parsing the In parameters array");

            if (whereValue.Count() > OracleConfiguration.WHERE_IN_LIMIT)
                throw new LambdaVisitorException($"The WHERE IN condition cannot be used with an array of more than {OracleConfiguration.WHERE_IN_LIMIT} elements");

            _currentParameterName = "InParameter";

            _queryStringBuilder
                .Append("(")
                .Append($"{_tableName}.{GetNameInDatabase.From(propertyName, _entityType)}")
                .Append(" IN (")
                .Append(AddParameterAndReturnQuery(_currentParameterName, whereValue))
                .Append("))");
        }
    }
}

