﻿using System;
using System.Reflection;

using Domain.Core.Attributes;

using OracleSqlBuilder.Exceptions;

namespace OracleSqlBuilder.Helpers
{
    internal class GetNameInDatabase
    {
        public static string From(PropertyInfo prop)
        {
            if (prop.GetCustomAttribute<NameInDatabaseAttribute>() != null)
                return prop.GetCustomAttribute<NameInDatabaseAttribute>().NameInDataBase;
            else
                return prop.Name.ConvertToOracleFieldName();
        }

        public static string TableNameFrom(Type entityType)
        {
            if (entityType.GetCustomAttribute<NameInDatabaseAttribute>() != null)
                return entityType.GetCustomAttribute<NameInDatabaseAttribute>().NameInDataBase;
            else
                return entityType.Name.ConvertToOracleFieldName();
        }

        public static string From(string propName, Type entityType)
        {
            if (entityType.GetProperty(propName) is null)
                throw new SqlBuilderException("The parameter is not a property of the entity");

            return From(entityType.GetProperty(propName));
        }
    }
}
