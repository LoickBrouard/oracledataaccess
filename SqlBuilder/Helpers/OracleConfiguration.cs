﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace OracleSqlBuilder.Helpers
{
    /// <summary>
    /// All Oracle specificities
    /// </summary>
    public static class OracleConfiguration
    {
        /// <summary>
        /// Symbol for Oracle parameter
        /// </summary>
        public const string SYMBOL_PARAMETER = ":";

        /// <summary>
        /// Symbol for Oracle scheme name
        /// </summary>
        public const string SCHEME_NAME = "SCHEME";

        /// <summary>
        /// Default size for stored procedure inputs
        /// </summary>
        public const int PARAMETER_DEFAUT_SIZE = 255;

        /// <summary>
        /// Default size for stored procedure inputs
        /// </summary>
        public const string SERVER_DATE_NAME = "sysdate";
        
        /// <summary>
        /// Default logical operator for null comparison
        /// </summary>
        public const string NULL_LOGICAL_OPERATOR = "IS";
        
        /// <summary>
        /// Default logical operator for null comparison
        /// </summary>
        public const string NULL_VALUE = "NULL";
        
        /// <summary>
        /// Default logical operator for null comparison
        /// </summary>
        public const int WHERE_IN_LIMIT = 100;

        /// <summary>
        /// Transform a C# Expression type into an equivalent Oracle symbol
        /// </summary>
        public static Dictionary<ExpressionType, string> LOGICAL_OPERATORS = new Dictionary<ExpressionType, string>
        {
            [ExpressionType.AndAlso] = "and",
            [ExpressionType.And] = "and",
            [ExpressionType.Or] = "or",
            [ExpressionType.OrElse] = "or",
            [ExpressionType.Equal] = "=",
            [ExpressionType.NotEqual] = "!=",
            [ExpressionType.Not] = "!=",
            [ExpressionType.GreaterThan] = ">",
            [ExpressionType.GreaterThanOrEqual] = ">=",
            [ExpressionType.LessThan] = "<",
            [ExpressionType.LessThanOrEqual] = "<=",
            [ExpressionType.Convert] = "",
        };

        /// <summary>
        /// To convert property name into Oracle field name.
        /// "_" symbol is inserted between every lower case and upper case character
        /// </summary>
        /// <param name="value"></param>
        /// <exemple>DatabaseField becomes Database_Field</exemple>
        /// <returns></returns>
        public static string ConvertToOracleFieldName(this string value) => string.Concat(value.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x.ToString() : x.ToString())).ToLower();

        /// <summary>
        /// Extract the procedure name from the object with scheme name
        /// "_" symbol is inserted between every lower case and upper case character
        /// StoredProcedureName becomes Scheme.Stored_Procedure_Name
        /// </summary>
        /// <typeparam name="TStoredProcedure"></typeparam>
        /// <returns></returns>
        public static string GetStoredProcedureName(Type type) => $"{SCHEME_NAME}.{type.Name.ConvertToOracleFieldName()}";

        public static string ReturnParameterNameWhithSymbol(string name)
        {
            return SYMBOL_PARAMETER + name;
        }
    }
}
