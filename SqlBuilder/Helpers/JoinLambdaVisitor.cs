﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using OracleSqlBuilder.Exceptions.LambdaVisitorExceptions;

namespace OracleSqlBuilder.Helpers
{
    // This class has been adapted from : https://www.codeproject.com/Articles/1241363/Expression-Tree-Traversal-Via-Visitor-Pattern-in-P

    internal class JoinLambdaVisitor : ExpressionVisitor
    {
        private static Stack<string> joinOnProperty = new Stack<string>();

        private JoinLambdaVisitor()
        {
        }

        //entry point
        public static (string, string) GetJoinExpressionFromPredicate<TEntity, TJoinedEntity>(LambdaExpression predicate)
        {
            JoinLambdaVisitor joinLambdaVisitor = new JoinLambdaVisitor();

            //Here this is Expression
            //We not pass whole predicate, just Body, because we not need predicate.Parameters: "x =>" part

            if (predicate.Body is BinaryExpression)
            {
                joinLambdaVisitor.Visit(predicate.Body);

                if (joinOnProperty.Count != 2)
                    throw new JoinLambdaVisitorException("Cannot found two variables. The expression must be of type entity1.field == entity2.field");

                string secondEntity = joinOnProperty.Pop();
                string firstEntity = joinOnProperty.Pop();

                if(typeof(TEntity).GetProperties().Where(p => p.Name == secondEntity || p.Name == firstEntity).Count() < 1)
                    throw new JoinLambdaVisitorException("One member is not an Entity property. The expression must be of type entity1.field == entity2.field");

                if(typeof(TJoinedEntity).GetProperties().Where(p => p.Name == secondEntity || p.Name == firstEntity).Count() < 1)
                    throw new JoinLambdaVisitorException("One member is not an Entity property. The expression must be of type entity1.field == entity2.field");

                return (firstEntity, secondEntity);
            }
            else
                throw new JoinLambdaVisitorException("The expression is not of binary type. The expression must be of type entity1.field == entity2.field");
                
        }

        //corresponds to: and, or, greater than, less than, etc.
        protected override Expression VisitBinary(BinaryExpression node)
        {
            Visit(node.Left);
            Visit(node.Right);
            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            joinOnProperty.Push(node.Member.Name);
            return node;
        }
    }
}
