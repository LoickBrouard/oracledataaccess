﻿using System;
using System.Linq.Expressions;

using Domain.Core.Primitives;

using OracleSqlBuilder.Models;
using OracleSqlBuilder.Exceptions;

namespace OracleSqlBuilder.Interfaces
{

    public interface ISelectStage<TEntity> where TEntity : Entity
    {
        IFilterStage<TEntity> Where(Expression<Func<TEntity, bool>> expression);

        /// <summary>
        /// To create the "WHERE xxx=:parameter" statement for the primary key
        /// To add to the parameter dictionary the list of variables
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NullPrimaryKeyException"></exception>
        /// <exception cref="PrimaryKeyAttributeMissingException"></exception>
        /// <exception cref="TooManyPrimaryKeyException"></exception>
        IFilterStage<TEntity> WhereId(object id);

        /// <summary>
        /// To create the "WHERE xxx=:param1 and yyy=:param2" statement for each primary key composing the composite key
        /// To add to the parameter dictionary the list of variables
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NullPrimaryKeyException"></exception>
        /// <exception cref="PrimaryKeyAttributeMissingException"></exception>
        /// <exception cref="TooManyPrimaryKeyException"></exception>
        IFilterStage<TEntity> WhereIds(TEntity entity);
      
        Query Build();
    }

    public interface ISelectJoinStage<TEntity> where TEntity : Entity
    {
        IJoinedStage<TEntity> Join<TJoinedEntity>(Join joinType, Expression<Func<TEntity, TJoinedEntity, bool>> expression) where TJoinedEntity : Entity;
    }

    public interface IInsertStage<TEntity>
    {
        Query Build();
    }

    public interface IPreInsertStage<TEntity>
    {
        IInsertStage<TEntity> ReturnElementsInsertedAutomatically();
        Query Build();
    }

    public interface IUpdateOrDeleteStage<TEntity>
    {
        /// <summary>
        /// To create the "WHERE xxx=:parameter" statement for the primary key
        /// To add to the parameter dictionary the list of variables
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NullPrimaryKeyException"></exception>
        /// <exception cref="PrimaryKeyAttributeMissingException"></exception>
        /// <exception cref="TooManyPrimaryKeyException"></exception>
        IFilterStage<TEntity> WhereId(object id);

        /// <summary>
        /// To create the "WHERE xxx=:param1 and yyy=:param2" statement for each primary key composing the composite key
        /// To add to the parameter dictionary the list of variables
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NullPrimaryKeyException"></exception>
        /// <exception cref="PrimaryKeyAttributeMissingException"></exception>
        /// <exception cref="TooManyPrimaryKeyException"></exception>
        IFilterStage<TEntity> WhereIds(TEntity entity);
    }

    public interface IFilterStage<TEntity>
    {
        Query Build();
    }
    public interface IJoinedStage<TEntity>
    {
        IFilterStage<TEntity> Where(Expression<Func<TEntity, bool>> expression);

        /// <summary>
        /// To create the "WHERE xxx=:parameter" statement for the primary key
        /// To add to the parameter dictionary the list of variables
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NullPrimaryKeyException"></exception>
        /// <exception cref="PrimaryKeyAttributeMissingException"></exception>
        /// <exception cref="TooManyPrimaryKeyException"></exception>
        IFilterStage<TEntity> WhereId(object id);

        Query Build();
    }

}
