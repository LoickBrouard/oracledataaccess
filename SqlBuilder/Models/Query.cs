﻿using System.Collections.Generic;

namespace OracleSqlBuilder.Models
{
    public class Query
    {
        public Dictionary<string, object> WhereFilter { get; set; }
        public string QueryString { get; private set; }
        public string SplitOn { get; set; }

        public Query(string queryString)
        {
            QueryString = queryString;
        }

        public Query(string queryString, Dictionary<string, object> whereFilter)
        {
            WhereFilter = whereFilter;
            QueryString = queryString;
        }
    }
}
