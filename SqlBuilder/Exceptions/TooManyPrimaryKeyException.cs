﻿using System;

namespace OracleSqlBuilder.Exceptions
{
    [Serializable]
    public class TooManyPrimaryKeyException : SqlBuilderException
    {
        public TooManyPrimaryKeyException() { }
    }
}
