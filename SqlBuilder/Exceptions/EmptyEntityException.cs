﻿using System;

namespace OracleSqlBuilder.Exceptions
{

	[Serializable]
	public class EmptyEntityException : SqlBuilderException
    {
		public EmptyEntityException() { }
	}
}
