﻿using System;

namespace OracleSqlBuilder.Exceptions
{

	[Serializable]
	public class PrimaryKeyAttributeMissingException : SqlBuilderException
    {
		public PrimaryKeyAttributeMissingException() { }
	}
}
