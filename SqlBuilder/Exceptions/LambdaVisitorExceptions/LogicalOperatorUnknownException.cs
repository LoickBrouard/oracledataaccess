﻿namespace OracleSqlBuilder.Exceptions.LambdaVisitorExceptions
{
    public class LogicalOperatorUnknownException : LambdaVisitorException
    {
        public string LogicalOperator { get; set; }
        public LogicalOperatorUnknownException(string logicalOperator) : base($"The logical operator {logicalOperator} cannot be used") { LogicalOperator = logicalOperator; }
    }
}
