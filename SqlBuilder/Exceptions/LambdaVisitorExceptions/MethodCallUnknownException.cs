﻿namespace OracleSqlBuilder.Exceptions.LambdaVisitorExceptions
{
    public class MethodCallUnknownException : LambdaVisitorException
    {
        public string MethodName { get; set; }
        public MethodCallUnknownException(string methodName) : base($"The method {methodName} cannot be used directly in the lambda expression.") { MethodName = methodName; }
    }
}
