﻿using System;

namespace OracleSqlBuilder.Exceptions.LambdaVisitorExceptions
{

    [Serializable]
    public class LambdaVisitorException : Exception
    {
        public LambdaVisitorException(string message) : base(message) { }
    }
}
