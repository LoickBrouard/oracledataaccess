﻿using System;

namespace OracleSqlBuilder.Exceptions.LambdaVisitorExceptions
{

	[Serializable]
	public class JoinLambdaVisitorException : SqlBuilderException
	{
		public JoinLambdaVisitorException(string message) : base(message) { }

	}
}
