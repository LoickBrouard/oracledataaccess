﻿using System;

namespace OracleSqlBuilder.Exceptions
{

	[Serializable]
	public class SqlBuilderException : Exception
	{
		public SqlBuilderException() { }
		public SqlBuilderException(string message) : base(message) { }
		public SqlBuilderException(string message, Exception inner) : base(message, inner) { }
	}
}
