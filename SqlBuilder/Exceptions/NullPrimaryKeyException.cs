﻿using System;

namespace OracleSqlBuilder.Exceptions
{

	[Serializable]
	public class NullPrimaryKeyException : SqlBuilderException
    {
		public NullPrimaryKeyException() { }
	}
}
